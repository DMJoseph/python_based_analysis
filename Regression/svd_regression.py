import numpy as np
import matplotlib.pyplot as plt
import os
import mat73
from sklearn.linear_model import Ridge, TweedieRegressor, Lasso
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import pandas as pd
import tkinter as tk
from tkinter import filedialog
import patsy
import copy
from third_regressions_the_charm import pick_formula, fit_model
from scipy.io import savemat


def run_models(data_path, output_path, model_type='ridge'):

    saved_mat_data = mat73.loadmat(data_path)

    neural_data = saved_mat_data['storedData']['spikeData']
    predictors = saved_mat_data['storedData']['designMatrix']
    predictor_names_list = saved_mat_data['storedData']['predictorNames']
    predictor_names = [x[0] for x in predictor_names_list]
    trial_numbers = saved_mat_data['storedData']['trialNumbers']

    behave_formula = pick_formula(predictor_names[:], ['laser', 'visual', 'attention', 'vis_att', 'laser_att'])

    SVD_timeseries = saved_mat_data['storedData']['V']
    SVs = saved_mat_data['storedData']['S']
    nan_filter = saved_mat_data['storedData']['nanIndex']

    predicted_v = []
    predicted_data = []
    selected_SVs = []

    for pp in range(len(neural_data)):

        S = np.diag(SVs[pp])
        S_percent = np.cumsum(S)/np.sum(S)
        num_SVCs = np.where(S_percent >= 0.90)[0][0]

        #plt.figure()
        #plt.plot(S_percent)

        # Allow user input to select the number of SVs
        #selection = plt.ginput(1)
        #num_SVCs = int(np.round(selection[0][0]))

        V = SVD_timeseries[pp][:, 0:num_SVCs]

        design_matrix = predictors[pp][0]
        if np.shape(design_matrix)[0] > np.shape(nan_filter[pp])[0]:
            design_matrix = design_matrix[:-1, :]

        if np.shape(design_matrix)[0] < np.shape(nan_filter[pp])[0]:
            design_matrix = np.r_[design_matrix, np.zeros([1, np.shape(design_matrix)[1]])]

        X = design_matrix[np.logical_not(nan_filter[pp]), :]
        predictor_data = pd.DataFrame(X, columns=predictor_names)

        feature_data = patsy.dmatrix(behave_formula[7:], data=predictor_data, return_type='dataframe')
        r2, validated_model = fit_model(feature_data, V, model_type)

        new_V = validated_model.predict(feature_data)

        U = saved_mat_data['storedData']['U'][pp][:, 0:num_SVCs]
        # old_data = U @ SVs[pp][0:num_SVCs,0:num_SVCs] @ np.transpose(V)

        no_nan_prediction = U @ SVs[pp][0:num_SVCs, 0:num_SVCs] @ np.transpose(new_V)
        nan_prediction = np.full([np.shape(no_nan_prediction)[0], np.shape(nan_filter[pp])[0]], np.NaN)
        nan_prediction[:, np.logical_not(nan_filter[pp])] = no_nan_prediction

        predicted_data.append(nan_prediction)
        #predicted_data.append(no_nan_prediction)
        predicted_v.append(new_V)
        selected_SVs.append(num_SVCs)

        '''
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.imshow(old_data[:, 2000:2500])
        plt.subplot(3, 1, 2)
        plt.imshow(behave_data[:, 2000:2500])
        plt.subplot(3, 1, 3)
        plt.imshow(np.transpose(neural_data[pp][0])[:, 2000:2500])
        
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.imshow(nan_prediction[:, 2000:2500])
        plt.subplot(2, 1, 2)
        plt.imshow(np.transpose(neural_data[pp][0])[:, 2000:2500])
        
        '''

    saved_mat_data['storedData']['predicted_data'] = predicted_data
    saved_mat_data['storedData']['predicted_v'] = predicted_v
    saved_mat_data['storedData']['selected_SVs'] = selected_SVs

    save_data_array = np.array(predicted_data, dtype=object)
    save_dict = {'behave_predictions': save_data_array}

    '''
    a = np.ones([5, 4])
    b = np.repeat(a, 5)

    a[:, 1] = np.NaN
    b = np.ones([5, 7])
    c = [a, b]
    d = [a]*10
    array_of_as = np.array(c[:], dtype=object)
    data_to_save = {'behave_predictions:': a}
    test_dict = {'a': array_of_as}
    #savemat(output_path, test_dict)
    '''
    savemat(output_path, save_dict)


if __name__ == "__main__":

    # default to 'full_set', Analysis uses 'behaviour'
    #model_to_run = 'full_set'
    #model_kind = 'MLP'

    #print('Running models: ' + model_to_run)

    root = tk.Tk()
    root.withdraw()

    folder_path = filedialog.askdirectory()
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)
        break

    for ii in range(len(filenames)):
        session_path = os.path.join(dirpath, filenames[ii])
        if any(map((lambda value: value in session_path), ('.csv', '.png', '.pkl', '_SVD_predictions'))):
            continue

        output_path = session_path[:-34] + '_SVD_predictions.mat'

        if os.path.exists(output_path):
            print('Model already run for: ' + session_path)
        else:
            print('Running models for: ' + session_path)
            run_models(session_path, output_path, model_type='Ridge')
