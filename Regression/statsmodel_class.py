from sklearn.base import BaseEstimator, RegressorMixin
import statsmodels.formula.api as smf
import statsmodels.api as sm


class StatsModel(BaseEstimator, RegressorMixin):
    def __init__(self, sm_class, formula):
        self.sm_class = sm_class
        self.formula = formula
        self.model = None
        self.result = None

    def fit(self, data, dummy):
        self.model = self.sm_class(formula=self.formula, data=data, family=sm.families.Gaussian())
        self.result = self.model.fit()

    def predict(self, X):
        return self.result.predict(X)
