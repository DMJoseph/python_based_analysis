import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import FactorAnalysis, PCA, NMF
from datetime import datetime
import matplotlib.pyplot as plt
import os
import mat73
from sklearn.linear_model import Ridge, TweedieRegressor
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.impute import SimpleImputer
import pandas as pd
import seaborn as sns
import tkinter as tk
from tkinter import filedialog
import statsmodels.api as sm
from statsmodels.formula.api import glm
from statsmodel_class import StatsModel
from scipy.stats import chi2
import patsy
import sys
import warnings
import getopt


def run_models(data_path, model_type='Gaussian'):
    saved_mat_data = mat73.loadmat(data_path)

    neural_data = saved_mat_data['storedData']['spikeData']
    predictors = saved_mat_data['storedData']['designMatrix']
    predictor_names_list = saved_mat_data['storedData']['predictorNames']
    predictor_names = [x[0] for x in predictor_names_list]
    trial_numbers = saved_mat_data['storedData']['trialNumbers']

    # predictor_names.append('spks')
    likelihood = []
    deviance = []
    aic = []
    predict_dict = {'full': [], 'laser': [], 'attention': [], 'behave': [], 'inter': [], 'laser_inter': [], 'visual': []}
    for pp in range(len(neural_data)):
        X = predictors[pp][0]
        predictor_data = pd.DataFrame(X, columns=predictor_names)

        # for col in predictor_data.columns:
        #    if 'v' in col or 'laser' in col or 'attend' in col:
        #       predictor_data[col] = predictor_data[col].astype("category")

        plane_data = neural_data[pp][0]
        if np.shape(plane_data)[0] == 2:
            warnings.warn('Empty plane. Skipping')
            continue

        if len(np.shape(plane_data)) == 1:
            neuron_number = 1
            plane_data = np.reshape(plane_data, [len(plane_data), -1])
        else:
            neuron_number = np.shape(plane_data)[1]

        for nn in range(neuron_number):
            print('Neuron: ' + str(nn) + ', Plane: ' + str(pp))
            neuron = plane_data[:, nn]
            spks = np.reshape(neuron, (len(neuron), -1))

            predictor_data['spks'] = spks

            temp_likelihood = []
            temp_deviance = []
            temp_aic = []

            full_formula = pick_formula(predictor_data.columns, 'all')
            full_like, full_dev, full_aic, full_p, full_pred = fit_model(full_formula, predictor_data, model_type)
            temp_likelihood.append(full_like)
            temp_deviance.append(full_dev)
            temp_aic.append(full_aic)
            predict_dict['full'].append(full_pred)

            no_laser_formula = pick_formula(predictor_data.columns, ['visual', 'behaviour', 'attention', 'vis_att'])
            laser_like, laser_dev, laser_aic, laser_p, laser_pred = fit_model(no_laser_formula, predictor_data, model_type)
            temp_likelihood.append(laser_like)
            temp_deviance.append(laser_dev)
            temp_aic.append(laser_aic)
            predict_dict['laser'].append(laser_pred)

            no_attention_formula = pick_formula(predictor_data.columns, ['laser', 'visual', 'behaviour'])
            attention_like, attention_dev, attention_aic, attention_p, attend_pred = fit_model(no_attention_formula, predictor_data, model_type)
            temp_likelihood.append(attention_like)
            temp_deviance.append(attention_dev)
            temp_aic.append(attention_aic)
            predict_dict['attention'].append(attend_pred)

            no_behaviour_formula = pick_formula(predictor_data.columns,
                                                ['laser', 'visual', 'attention', 'vis_att', 'laser_att'])
            behave_like, behave_dev, behave_aic, behave_p, behave_pred = fit_model(no_behaviour_formula, predictor_data, model_type)
            temp_likelihood.append(behave_like)
            temp_deviance.append(behave_dev)
            temp_aic.append(behave_aic)
            predict_dict['behave'].append(behave_pred)

            no_interaction_formula = pick_formula(predictor_data.columns, ['laser', 'visual', 'behaviour', 'attention'])
            inter_like, inter_dev, inter_aic, inter_p, inter_pred = fit_model(no_interaction_formula, predictor_data, model_type)
            temp_likelihood.append(inter_like)
            temp_deviance.append(inter_dev)
            temp_aic.append(inter_aic)
            predict_dict['inter'].append(inter_pred)

            no_laser_interaction_formula = pick_formula(predictor_data.columns,
                                                        ['laser', 'visual', 'behaviour', 'attention', 'vis_att'])
            la_like, la_dev, la_aic, la_p, la_pred = fit_model(no_laser_interaction_formula, predictor_data, model_type)
            temp_likelihood.append(la_like)
            temp_deviance.append(la_dev)
            temp_aic.append(la_aic)
            predict_dict['laser_inter'].append(la_pred)

            no_visual_formula = pick_formula(predictor_data.columns, ['laser', 'behaviour', 'attention', 'laser_att'])
            no_visual_activity, no_visual_feature_data = patsy.dmatrices(no_visual_formula,
                                                                         data=predictor_data,
                                                                         return_type='dataframe')
            vis_like, vis_dev, vis_aic, vis_p, vis_pred = fit_model(no_visual_formula, predictor_data, model_type)
            temp_likelihood.append(vis_like)
            temp_deviance.append(vis_dev)
            temp_aic.append(vis_aic)
            predict_dict['visual'].append(vis_pred)

            likelihood.append(temp_likelihood)
            deviance.append(temp_deviance)
            aic.append(temp_aic)


    param_numbers = {'full':full_p, 'laser':laser_p, 'attention':attention_p, 'behave':behave_p, 'inter':inter_p, 'laser_inter':la_p, 'visual':vis_p}

    llf_df = pd.DataFrame(likelihood,
                             columns=['full', 'no_laser', 'no_attend', 'no_behave', 'no_inter', 'no_laser_inter',
                                      'no_visual'])
    dev_df = pd.DataFrame(deviance,
                          columns=['full', 'no_laser', 'no_attend', 'no_behave', 'no_inter', 'no_laser_inter',
                                   'no_visual'])
    aic_df = pd.DataFrame(aic,
                          columns=['full', 'no_laser', 'no_attend', 'no_behave', 'no_inter', 'no_laser_inter',
                                   'no_visual'])
    return llf_df, dev_df, aic_df, param_numbers, predict_dict


def pick_formula(predictor_tags, formula_list):
    if formula_list == 'all':
        formula_list = ['laser', 'visual', 'behaviour', 'attention', 'vis_att', 'laser_att']

    budding_formula = ''
    if 'laser' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + ') + ' for x in predictor_tags[8:12]])
    if 'visual' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + ') + ' for x in predictor_tags[:8]])
    if 'behaviour' in formula_list:
        budding_formula = budding_formula + ''.join(['standardize(' + x + ') + ' for x in predictor_tags[12:-2]])
    if 'vis_att' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + '):attend + ' for x in predictor_tags[:8]])
    if 'laser_att' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + '):attend + ' for x in predictor_tags[8:12]])

    if 'attention' in formula_list:
        full_formula = 'spks ~ ' + budding_formula + 'attend'
    else:
        full_formula = 'spks ~ ' + budding_formula[:-3]

    return full_formula


def fit_model(formula, data, model_type):
    # Set up and train ridge regression
    #train_X, test_X, train_y, test_y = train_test_split(X, spks, test_size=0.2, random_state=42)

    if model_type == 'Gaussian':
        glm_model = glm(formula=formula, data=data, family=sm.families.Gaussian())
        model = glm_model.fit()
    elif model_type == 'Poisson':
        glm_model = glm(formula=formula, data=data, family=sm.families.Poisson())
        model = glm_model.fit()
    elif model_type == 'NegBin':
        glm_model = glm(formula=formula, data=data, family=sm.families.NegativeBinomial())
        model = glm_model.fit()

    dev = model.deviance
    like = model.llf
    aic = model.aic
    p_num = model.df_model
    # print("Tuned ridge alpha: {}".format(gm_cv.best_params_))
    # print("Best score while training: {}".format(gm_cv.best_score_))
    verbose = 0
    if verbose:
        print("Deviance: {}".format(dev))
        print('Log likelihood: {}'.format(like))
        print('AIC: {}'.format(aic))

    predicted_data = model.predict(data)
    '''
    ridge_predict = gm_cv.predict(X)
    plt.figure()
    plt.plot(data['spks'])
    plt.plot(predicted_data)
    plt.legend(['Data', 'Prediction'])
    plt.show()
    '''

    return like, dev, aic, p_num, predicted_data


#http://sherrytowers.com/2019/03/18/determining-which-model-fits-the-data-significantly-better/#:~:text=If%20they%20are%20very%20close,likelihood%20ratio%20test%20is%20preferred.
def aic_relative_likelihood(aic1, aic2):
    min_aic = np.minimum(aic1, aic2)
    b1 = np.exp((min_aic-aic1)/2)
    b2 = np.exp((min_aic-aic2)/2)
    p1 = b1/(b1+b2)
    p2 = b2/(b1+b2)

    return p1, p2


def likelihood_ratio_test(l1, l2, k):
    lambda_stat = 2*(l1-l2)

    p = 1 - chi2.cdf(lambda_stat, k)
    return p


if __name__ == "__main__":
    # The first argument back from the command line is the name of the script 'si_header_cmd.py', so we can skip this.
    ops, args = getopt.getopt(sys.argv[1:], "i:", ["ifile="])
    if not ops:
        model_select = 'Gaussian'
    else:
        model_select = ops[0][1]

    print('Selected model is: ' + model_select)

    root = tk.Tk()
    root.withdraw()

    folder_path = filedialog.askdirectory()
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)

    # session_path = r'W:\Data\Dylan\tdt_controls\Processed\20210307_161759__CTL001_B2_SWITCHOP\iMag\regression_data.mat'

    llf_df_list = []
    dev_df_list = []
    aic_df_list = []

    for ii in range(len(filenames)):
        session_path = os.path.join(dirpath, filenames[ii])
        if any(map((lambda value: value in session_path), ('.csv', '.png', 'pkl'))):
            continue

        llf_save_path = os.path.join(dirpath, filenames[ii][:-20] + '__glm_dataframe_llf.csv')
        dev_save_path = os.path.join(dirpath, filenames[ii][:-20] + '__glm_dataframe_dev.csv')
        aic_save_path = os.path.join(dirpath, filenames[ii][:-20] + '__glm_dataframe_aic.csv')
        predictions_save_path = os.path.join(dirpath, filenames[ii][:-20] + '_model_predictions.csv')

        if os.path.exists(aic_save_path):
            print('Model already run for: ' + session_path)
            print('Loading DataFrames.')
            llf_df = pd.read_csv(llf_save_path, index_col=0)
            dev_df = pd.read_csv(dev_save_path, index_col=0)
            aic_df = pd.read_csv(aic_save_path, index_col=0)
        else:
            print('Running models for: ' + session_path)
            llf_df, dev_df, aic_df, param_numbers, predictions = run_models(session_path, model_type=model_select)


            llf_df['laser_LRT'] = likelihood_ratio_test(llf_df['full'], llf_df['no_laser'], param_numbers['full']-param_numbers['laser'])
            llf_df['attend_LRT'] = likelihood_ratio_test(llf_df['full'], llf_df['no_attend'], param_numbers['full']-param_numbers['attention'])
            llf_df['behave_LRT'] = likelihood_ratio_test(llf_df['full'], llf_df['no_behave'], param_numbers['full']-param_numbers['behave'])
            llf_df['inter_LRT'] = likelihood_ratio_test(llf_df['full'], llf_df['no_inter'], param_numbers['full']-param_numbers['inter'])
            llf_df['laser_inter_LRT'] = likelihood_ratio_test(llf_df['full'], llf_df['no_laser_inter'], param_numbers['full']-param_numbers['laser_inter'])
            llf_df['visual_LRT'] = likelihood_ratio_test(llf_df['full'], llf_df['no_visual'], param_numbers['full']-param_numbers['visual'])
            llf_df['session'] = filenames[ii][:-20]
            llf_df.to_csv(llf_save_path)

            dev_df.to_csv(dev_save_path)

            aic_df['full_laser_RL'], aic_df['laser_RL'] = aic_relative_likelihood(aic_df['full'], aic_df['no_laser'])
            aic_df['full_attend_RL'], aic_df['attend_RL'] = aic_relative_likelihood(aic_df['full'], aic_df['no_attend'])
            aic_df['full_behave_RL'], aic_df['behave_RL'] = aic_relative_likelihood(aic_df['full'], aic_df['no_behave'])
            aic_df['full_inter_RL'], aic_df['inter_RL'] = aic_relative_likelihood(aic_df['full'], aic_df['no_inter'])
            aic_df['full_laser_inter_RL'], aic_df['laser_inter_RL'] = aic_relative_likelihood(aic_df['full'], aic_df['no_laser_inter'])
            aic_df['full_visual_RL'], aic_df['visual_RL'] = aic_relative_likelihood(aic_df['full'], aic_df['no_visual'])
            aic_df['session'] = filenames[ii]

            aic_df.to_csv(aic_save_path)
            print('Saved single session models')

        llf_df_list.append(llf_df)
        dev_df_list.append(dev_df)
        aic_df_list.append(aic_df)

    full_llf_df = pd.concat(llf_df_list[:])
    #full_llf_df.to_pickle(os.path.join(dirpath, model_select + '__glm_dataframe_llf'))
    full_llf_df.to_csv(os.path.join(dirpath, model_select + '__glm_dataframe_llf.csv'))

    full_dev_df = pd.concat(dev_df_list[:])
    #full_dev_df.to_pickle(os.path.join(dirpath, model_select + '_glm_dataframe_dev'))
    full_dev_df.to_csv(os.path.join(dirpath, model_select + '__glm_dataframe_dev.csv'))

    full_aic_df = pd.concat(aic_df_list[:])
    #full_aic_df.to_pickle(os.path.join(dirpath, model_select + '_glm_dataframe_aic'))
    full_aic_df.to_csv(os.path.join(dirpath, model_select + '__glm_dataframe_aic.csv'))
    print('Saved concatenated dataframes, done.')

    #sns.boxplot(data=full_df[['laser_diff', 'attend_diff', 'behave_diff', 'inter_diff', 'laser_inter_diff', 'visual_diff']])
    #plt.show()