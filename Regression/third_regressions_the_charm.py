import sys
import getopt
import numpy as np
import matplotlib.pyplot as plt
import os
import mat73
from sklearn.linear_model import Ridge, TweedieRegressor, Lasso
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import pandas as pd
import tkinter as tk
from tkinter import filedialog
import patsy
import copy


def run_models(data_path, model_type='ridge', requested_models='full_set'):
    saved_mat_data = mat73.loadmat(data_path)

    nan_index = saved_mat_data['storedData']['nanIndex']
    neural_data = saved_mat_data['storedData']['spikeData']
    predictors = saved_mat_data['storedData']['designMatrix']
    predictor_names_list = saved_mat_data['storedData']['predictorNames']
    predictor_names = [x[0] for x in predictor_names_list]
    trial_numbers = saved_mat_data['storedData']['trialNumbers']

    ingredients = predictor_names[:]
    ingredients.append('spks')

    if requested_models == 'full_set':
        formula_list = {'full': pick_formula(ingredients, 'all'),
                        # Model without laser terms
                        # 'no_laser': pick_formula(ingredients, ['visual', 'behaviour', 'attention', 'vis_att', 'odour']),
                        # # Model without attention
                        # 'no_attend': pick_formula(ingredients, ['laser', 'visual', 'behaviour', 'odour']),
                        # Model without behaviour
                        'no_behave': pick_formula(ingredients, ['laser', 'visual', 'attention', 'vis_att', 'laser_att', 'odour'])}
                        # Model without attention interaction terms
                        # 'no_inter': pick_formula(ingredients, ['laser', 'visual', 'behaviour', 'attention', 'odour']),
                        # # Model without laser-attention interaction term
                        # 'no_laser_inter': pick_formula(ingredients, ['laser', 'visual', 'behaviour', 'attention', 'vis_att', 'odour']),
                        # # Model without visual
                        # 'no_visual': pick_formula(ingredients, ['laser', 'behaviour', 'attention', 'laser_att', 'odour']),
                        # # Model without visual-attention interaction term
                        # 'no_vis_inter': pick_formula(ingredients, ['laser', 'behaviour', 'attention', 'laser_att', 'odour']),
                        # # Model without visual
                        # 'no_odour': pick_formula(ingredients, ['behaviour', 'visual', 'attention', 'vis_att'])}

        # formula_list = {'full': pick_formula(ingredients, 'all'),
        #                 # Model without attention
        #                 'no_attend': pick_formula(ingredients, ['visual', 'behaviour', 'odour']),
        #                 # Model without behaviour
        #                 'no_behave': pick_formula(ingredients, ['visual', 'attention', 'vis_att', 'odour']),
        #                 # Model without attention interaction terms
        #                 'no_inter': pick_formula(ingredients, ['visual', 'behaviour', 'attention', 'odour']),
        #                 # Model without visual
        #                 'no_visual': pick_formula(ingredients, ['behaviour', 'attention', 'odour']),
        #                 # Model without visual
        #                 'no_odour': pick_formula(ingredients, ['behaviour', 'visual', 'attention', 'vis_att'])}
    else:
        formula_list = {requested_models: pick_formula(ingredients, requested_models)}

    # Make empty dictionaries for later
    model_data = {}
    scores = {}
    for key in formula_list.keys():
        model_data[key] = []
        scores[key] = []

    for pp in range(len(neural_data)):
        X = predictors[pp][0][np.logical_not(nan_index[pp]), :]
        predictor_data = pd.DataFrame(X, columns=predictor_names)

        # for col in predictor_data.columns:
        #    if 'v' in col or 'laser' in col or 'attend' in col:
        #       predictor_data[col] = predictor_data[col].astype("category")

        plane_data = neural_data[pp][0][np.logical_not(nan_index[pp]), :]
        total_rois = np.shape(plane_data)[1]
        for nn in range(np.shape(plane_data)[1]):
            print('Neuron: ' + str(nn) + ' of ' + str(total_rois) + ', Plane: ' + str(pp))
            neuron = plane_data[:, nn]
            spks = np.reshape(neuron, (len(neuron), -1))

            predictor_data['spks'] = spks

            for key, value in formula_list.items():

                print('Fitting ' + key + ' model:')

                activity, feature_data = patsy.dmatrices(value, data=predictor_data, return_type='dataframe')
                r2, validated_model = fit_model(feature_data, activity, model_type)
                model_data[key].append(validated_model)
                scores[key].append(r2)

    scores_df = pd.DataFrame.from_dict(scores)
    model_df = pd.DataFrame.from_dict(model_data)

    return scores_df, model_df


def pick_formula(predictor_tags, formula_list):
    if formula_list == 'all':
        formula_list = ['laser', 'visual', 'behaviour', 'attention', 'vis_att', 'laser_att', 'odour']
        #formula_list = ['visual', 'behaviour', 'attention', 'vis_att', 'odour']

    budding_formula = ''
    if 'laser' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + ') + ' for x in predictor_tags if 'laser' in x])
    if 'visual' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + ') + ' for x in predictor_tags if np.logical_or('v1' in x, 'v2' in x)])
    if 'behaviour' in formula_list:
        budding_formula = budding_formula + ''.join(['standardize(' + x + ') + ' for x in predictor_tags if np.logical_or('lick' in x, 'run' in x)]) #'run' in x]) # np.logical_or('lick' in x, 'run' in x)])
    if 'vis_att' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + '):attend + ' for x in predictor_tags if np.logical_or('v1' in x, 'v2' in x)])
    if 'odour' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + ') + ' for x in predictor_tags if np.logical_or('o1' in x, 'o2' in x)])
    if 'laser_att' in formula_list:
        budding_formula = budding_formula + ''.join(['C(' + x + '):attend + ' for x in predictor_tags if 'laser' in x])

    if 'attention' in formula_list:
        full_formula = 'spks ~ ' + budding_formula + 'attend'
    else:
        full_formula = 'spks ~ ' + budding_formula[:-3]

    return full_formula


def fit_model(X, spks, model_type):
    # Set up and train ridge regression
    train_X, test_X, train_y, test_y = train_test_split(X, spks, test_size=0.2, random_state=42)

    if model_type == 'Ridge':
        steps = [('ridge', Ridge())]
        alphas = {'ridge__alpha': np.logspace(-4, 0, 50)}
    elif model_type =='Lasso':
        steps = [('lasso', Lasso())]
        alphas = {'lasso__alpha': np.logspace(-4, 0, 50)}
    elif model_type == 'Poisson':
        steps = [('tweedie', TweedieRegressor(power=1, link='log'))]
        alphas = {'tweedie__alpha': np.logspace(-4, 0, 50)}
    elif model_type == 'Gamma':
        steps = [('tweedie', TweedieRegressor(power=2, link='log'))]
        alphas = {'tweedie__alpha': np.logspace(-4, 0, 50)}
    elif model_type == 'MLP':
        steps = [('MLP', MLPRegressor(random_state=42, hidden_layer_sizes=(100, 100, 100), max_iter=1000))]
        alphas = {'MLP__alpha': np.logspace(-4, 0, 5)}

    pipe = Pipeline(steps)
    gm_cv = GridSearchCV(pipe, alphas, cv=5)

    if model_type == 'MLP':
        flat_y = np.ravel(train_y)
        gm_cv.fit(train_X, flat_y)
    else:
        gm_cv.fit(train_X, train_y)

    r2 = gm_cv.score(test_X, test_y)

    # print("Tuned ridge alpha: {}".format(gm_cv.best_params_))
    # print("Best score while training: {}".format(gm_cv.best_score_))
    print("Tuned R squared on hold out data: {}".format(r2))

    if 0:
        ridge_predict = gm_cv.predict(X)
        plt.figure()
        plt.plot(spks)
        plt.plot(ridge_predict)
        plt.legend(['Data', 'Prediction'])
        plt.ylabel('dF/F')
        plt.xlabel('Data index')
        plt.show()

    return r2, gm_cv


if __name__ == "__main__":

    # Fetch the arguments given from the command line, allowing the option -f for the model fit and -p for the parameters.

    ops, args = getopt.getopt(sys.argv[1:], "f:p:")
    print(ops)
    # default to 'full_set', Analysis uses 'behaviour'
    if not ops:
        model_to_run = 'full_set'
        model_kind = 'Ridge'
    else:
        if ops[0][0] == '-p':
            model_to_run = ops[0][1]
        else:
            model_to_run = 'full_set'

        if ops[1][0] == '-f':
            model_kind = ops[1][1]
        else:
            model_kind = 'Ridge'

    print('Running models: ' + model_to_run)
    root = tk.Tk()
    root.withdraw()

    folder_path = filedialog.askdirectory()
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)
        break

    scores_list = []
    models_list = []
    for ii in range(len(filenames)):
        session_path = os.path.join(dirpath, filenames[ii])
        if any(map((lambda value: value in session_path), ('.csv', '.png', '.pkl', '_whole', '_activity_prediction'))):
            continue

        scores_out_path = os.path.join(dirpath, filenames[ii][:-36] + '_' + model_kind + '_regression_' + model_to_run + '_scores_dataframe.csv')
        model_out_path = os.path.join(dirpath, filenames[ii][:-36] + '_' + model_kind + '_regression_' + model_to_run + '_models_dataframe.pkl')

        if os.path.exists(scores_out_path):
            print('Model already run for: ' + session_path)
            print('Loading DataFrames.')
            df_scores = pd.read_csv(scores_out_path)
            df_models = pd.read_pickle(model_out_path)
        else:
            print('Running models for: ' + session_path)
            df_scores, df_models = run_models(session_path, model_type=model_kind, requested_models=model_to_run)

            if 'full' in df_scores.keys():
                for key in df_scores.keys():
                    if 'full' != key:
                        df_scores[key + '_diff'] = df_scores[key] - df_scores['full']

            df_scores['session'] = filenames[ii][:-36]
            df_models['session'] = filenames[ii][:-36]

            #if model_to_run == 'full_set':
            #    df_scores.to_csv(os.path.join(dirpath, filenames[ii][:-36] + '_ridge_regression_scores_dataframe.csv'))
            #    df_models.to_pickle(os.path.join(dirpath, filenames[ii][:-36] + '_ridge_regression_models_dataframe.pkl'))
            #else:
            df_scores.to_csv(scores_out_path)
            df_models.to_pickle(model_out_path)

        scores_list.append(df_scores)
        models_list.append(df_models)

    full_scores = pd.concat(scores_list[:])
    full_models = pd.concat(models_list[:])

    #if model_to_run == 'full_set':
    #    full_scores.to_csv(os.path.join(dirpath, 'full_ridge_regression_scores_dataframe.csv'))
    #    full_models.to_pickle(os.path.join(dirpath, 'full_ridge_regression_models_dataframe.pkl'))
    #else:
    full_scores.to_csv(os.path.join(dirpath, 'all_session_' + model_to_run + '_' + model_kind + '_regression_scores_dataframe.csv'))
    full_models.to_pickle(os.path.join(dirpath, 'all_session_' + model_to_run + '_' + model_kind + '_regression_models_dataframe.pkl'))
