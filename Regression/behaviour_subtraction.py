import pandas as pd
import numpy as np
import seaborn as sns
import tkinter as tk
from tkinter import filedialog
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge, TweedieRegressor
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
import os
import mat73
import patsy
from scipy.io import savemat
from third_regressions_the_charm import pick_formula

root = tk.Tk()
root.withdraw()

folder_path = filedialog.askdirectory()
f = []
for (dirpath, dirnames, filenames) in os.walk(folder_path):
    f.extend(filenames)
    break

all_files = [*map((lambda ff: os.path.join(dirpath, ff)), filenames)]

redo_step = False
behave_only_model = True
model_type = 'Ridge' #'MLP'
if behave_only_model:
    match_string = model_type + '_regression_behaviour_models_dataframe.pkl'
else:
    match_string = model_type + '_regression_models_dataframe.pkl'

for filename in all_files:
    if match_string not in filename:
        continue

    # Load the model data
    model_df = pd.read_pickle(filename)
    # Find the matching feature data file

    suffix_length = len(match_string)

    data_file = [name for name in all_files if (filename[:-suffix_length] in name) and
                 ('whole_session_regression_data.mat' in name)]
    output_filename = filename[:-suffix_length] + model_type + '_behaviour_activity_prediction.mat'

    if os.path.exists(output_filename) and not redo_step:
        print('Correction data already exists at: ' + output_filename + ' Skipping')
        continue

    saved_mat_data = mat73.loadmat(data_file[0])
    print('Loaded data from file: ' + data_file[0])

    neural_data = saved_mat_data['storedData']['spikeData']
    predictors = saved_mat_data['storedData']['designMatrix']
    predictor_names_list = saved_mat_data['storedData']['predictorNames']
    predictor_names = [x[0] for x in predictor_names_list]
    trial_numbers = saved_mat_data['storedData']['trialNumbers']
    nan_index = saved_mat_data['storedData']['nanIndex']

    ingredients = predictor_names.copy()
    ingredients.append('spks')

    if behave_only_model:
        formula = pick_formula(ingredients, 'behaviour')
        behave_models = model_df['behaviour']
    else:
        formula = pick_formula(ingredients, 'all')
        full_models = model_df['full']

    print('Running session for ' + filename[:-(suffix_length+1)])

    cc = 0
    all_behave = []
    for pp in range(len(neural_data)):
        X = predictors[pp][0]
        predictor_data = pd.DataFrame(X, columns=predictor_names)

        plane_data = neural_data[pp][0]

        while np.shape(plane_data)[0] < np.shape(predictor_data)[0]:
            predictor_data = predictor_data.drop(index=predictor_data.tail(1).index)
            print('Plane ' + str(pp) + ', predictor data is 1 index longer than neural data, trimming.')

        while np.shape(plane_data)[0] > np.shape(predictor_data)[0]:
            plane_data = plane_data[:-1, :]
            nan_index[pp] = nan_index[pp][:-1]
            print('Plane ' + str(pp) + ', neural data is 1 index longer than predictor data, trimming.')


        predicted_traces = np.full_like(np.transpose(plane_data), np.nan)

        for nn in range(np.shape(plane_data)[1]):
            print('Neuron: ' + str(nn) + ', Plane: ' + str(pp))

            predictor_data['spks'] = np.reshape(plane_data[:, nn], (len(plane_data[:, nn]), -1))
            activity, feature_data = patsy.dmatrices(formula, data=predictor_data, return_type='dataframe')

            if behave_only_model:
                predicted_traces[[nn], np.logical_not(nan_index[pp])] = np.transpose(behave_models[cc].predict(feature_data))
            else:
                non_behave_index = [all(map((lambda bb: bb not in col), ('Intercept', 'run', 'lick'))) for col in feature_data.columns]
                full_prediction = full_models[cc].predict(feature_data)
                full_models[cc].best_estimator_.named_steps.ridge.coef_[0][non_behave_index] = 0
                predicted_traces[[nn], :] = np.transpose(full_models[cc].predict(feature_data))

            cc += 1

            if False:
                plt.figure()
                plt.plot(activity)
                #plt.plot(full_prediction)
                plt.plot(predicted_traces[nn, :])
                #plt.legend(['Data', 'Full prediction', 'behave prediction'])
                plt.legend(['Data', 'behave prediction'])
                plt.ylabel('dF/F')
                plt.xlabel('Data index')
                plt.show()

        all_behave.append(predicted_traces)
        #all_behave[str(pp)] = predicted_traces

    save_data_array = np.array(all_behave[:], dtype=object)
    data_to_save = {'behave_predictions': save_data_array}
    savemat(output_filename, data_to_save)
