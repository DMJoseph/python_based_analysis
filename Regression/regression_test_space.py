import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import FactorAnalysis, PCA, NMF
from datetime import datetime
import matplotlib.pyplot as plt
import os
import mat73
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import cross_val_score


def display_plot(cv_scores, cv_scores_std):
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(alpha_space, cv_scores)

    std_error = cv_scores_std / np.sqrt(10)

    ax.fill_between(alpha_space, cv_scores + std_error, cv_scores - std_error, alpha=0.2)
    ax.set_ylabel('CV Score +/- Std Error')
    ax.set_xlabel('Alpha')
    ax.axhline(np.max(cv_scores), linestyle='--', color='.5')
    ax.set_xlim([alpha_space[0], alpha_space[-1]])
    ax.set_xscale('log')
    plt.show()


session_dir = r'E:\tdt_controls\Smaller_session_group\20210316_154451__CTL004_B2_SWITCHOP'
saved_mat_data = mat73.loadmat(os.path.join(session_dir, 'imgtrl_base_data.mat'))

trial_data = saved_mat_data['trial_data']
test_trial = trial_data[18, 50, :]

trial_time = saved_mat_data['trial_time']
time_index = np.logical_and(trial_time > -2, trial_time < 3)
test_trial = test_trial[time_index]

plt.figure()
plt.plot(trial_time[time_index], test_trial)
plt.show()

run_data = saved_mat_data['behave_data']['run']['allTrial'][18,:]
run_time = saved_mat_data['behave_data']['run']['relativeRunTime']
plt.figure()
plt.plot(run_time, run_data)
plt.show()

run_sample_idx = np.linspace(0, len(run_data)-1, len(time_index), dtype=int)
sampled_run_data = run_data[run_sample_idx]
sampled_run_time = run_time[run_sample_idx]

plt.figure()
plt.plot(sampled_run_time, sampled_run_data)
plt.show()

two_seconds = np.sum(np.logical_and(trial_time > -2, trial_time <= 0))
run_matrix = np.empty((len(test_trial), two_seconds))
a = np.where(time_index)
min_tt = np.min(a[0])
for tt in a[0]:
    run_matrix[tt-min_tt, :] = sampled_run_data[tt-two_seconds:tt]

plt.figure()
plt.plot(trial_time[time_index], run_matrix[:, -1])
plt.show()

stimulus = np.zeros((32,1))
stimulus[12:] = 1

constant = np.ones((32, 1))
X = np.concatenate((constant, stimulus, run_matrix), axis=1)
y = np.empty((32,1))
y[:,0] = test_trial

just_stim = np.empty((32,1))
just_stim[:,0] = X[:, 1]

clf = RidgeCV(normalize=True, cv=10).fit(X, y)
clf.score(X, y)
y_pred = clf.predict(X)
plt.figure()
plt.plot(trial_time[time_index], y_pred)
plt.show()
'''
# Setup the array of alphas and lists to store scores
alpha_space = np.logspace(-4, 0, 50)
ridge_scores = []
ridge_scores_std = []

# Create a ridge regressor: ridge
ridge = Ridge(normalize=True)

# Compute scores over range of alphas
for alpha in alpha_space:
    # Specify the alpha value to use: ridge.alpha
    ridge.alpha = alpha

    # Perform 10-fold CV: ridge_cv_scores
    ridge_cv_scores = cross_val_score(ridge, X, y, cv=5)

    # Append the mean of ridge_cv_scores to ridge_scores
    ridge_scores.append(np.mean(ridge_cv_scores))

    # Append the std of ridge_cv_scores to ridge_scores_std
    ridge_scores_std.append(np.std(ridge_cv_scores))

# Display the plot
display_plot(ridge_scores, ridge_scores_std)

best_alpha = alpha_space[ridge_scores == np.max(ridge_scores)]
ridge.alpha = best_alpha
ridge_cv_scores = cross_val_score(ridge, X, y, cv=5)

for rr in run_matrix:
    plt.figure()
    plt.plot(rr)
    plt.show()
'''
# Create model with: constant, stimulus, running speed time lagged, licking time lagged (-2 to 0s)