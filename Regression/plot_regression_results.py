import pandas as pd
import seaborn as sns
import tkinter as tk
from tkinter import filedialog
import matplotlib.pyplot as plt
import os

root = tk.Tk()
root.withdraw()

folder_path = filedialog.askdirectory()
f = []
for (dirpath, dirnames, filenames) in os.walk(folder_path):
    f.extend(filenames)

for ii in range(len(filenames)):
    data_path = os.path.join(dirpath, filenames[ii])
    if '.csv' not in data_path:
        continue

    df = pd.read_csv(data_path, index_col=0)

    if 'llf' in data_path:
        plt.figure(figsize=(12, 8))
        sns.boxplot(data=df[['laser_LRT', 'attend_LRT', 'behave_LRT', 'inter_LRT', 'laser_inter_LRT', 'visual_LRT']])
        plt.title(filenames[ii][:-23])
        plt.ylabel('Likelihood ratio test P values')
        plt.ylim((0, 0.05))
        plt.savefig(data_path[:-21] + 'llf_results.png')
        plt.show()
    elif 'aic' in data_path:
        plt.figure(figsize=(18, 8))
        sns.boxplot(data=df[['full_laser_RL', 'laser_RL', 'full_attend_RL', 'attend_RL', 'full_behave_RL', 'behave_RL',
                             'full_inter_RL', 'inter_RL', 'full_laser_inter_RL', 'laser_inter_RL', 'full_visual_RL',
                             'visual_RL']])
        plt.title(filenames[ii][:-23])
        plt.ylabel('AIC relative likelihood results')
        plt.savefig(data_path[:-21] + 'aic_results.png')
        plt.show()
    elif 'dev' in data_path:
        plt.figure(figsize=(12, 8))
        sns.boxplot(data=df)
        plt.title(filenames[ii][:-23])
        plt.ylabel('Deviance')
        plt.savefig(data_path[:-21] + 'dev_results.png')
        plt.show()
    elif 'ridge' in data_path:
        plt.figure(figsize=(12, 8))
        sns.boxplot(data=df[['laser_diff', 'attend_diff', 'behave_diff', 'inter_diff', 'laser_inter_diff', 'visual_diff']])
        plt.title(filenames[ii][:-14])
        plt.ylabel('Change in R^2')
        plt.savefig(data_path[:-14] + '.png')
        plt.show()
    elif 'Ridge' in data_path:
        plt.figure(figsize=(12, 8))
        sns.boxplot(data=df[['full', 'no_attend', 'behave', 'no_inter', 'no_visual']])
        #plt.title(df['session'][0])
        plt.title(filenames[ii][:-14])
        plt.ylabel('R^2')
        plt.savefig(data_path[:-14] + '_R2.png')
        plt.show()

        plt.figure(figsize=(12, 8))
        sns.boxplot(data=df[['no_attend_diff', 'behave_diff', 'no_inter_diff', 'no_visual_diff']])
        #plt.title(df['session'][0])
        plt.title(filenames[ii][:-14])
        plt.ylabel('Change in R^2')
        plt.savefig(data_path[:-14] + '_R2_change.png')
        plt.show()
