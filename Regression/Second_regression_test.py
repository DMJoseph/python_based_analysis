import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import FactorAnalysis, PCA, NMF
from datetime import datetime
import matplotlib.pyplot as plt
import os
import mat73
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.impute import SimpleImputer
import pandas as pd
import seaborn as sns
import tkinter as tk
from tkinter import filedialog
import statsmodels.api as sm
from statsmodels.formula.api import glm


def run_models(data_path):

    saved_mat_data = mat73.loadmat(data_path)

    neural_data = saved_mat_data['storedData']['data']
    predictors = saved_mat_data['storedData']['predictors']
    predictor_names = saved_mat_data['storedData']['predictorNames']

    r_squares_slice = []
    r_squares_no_laser = []
    r_squares_no_at = []
    coeffs_slice = []
    coeffs_lasso = []

    for pp in range(len(neural_data)):
        X = predictors[pp][0]

        # Remove all the lagged predictors for now
        #X = np.delete(X, [7,8,9,10,11,12,13,15,16,17,18,19,20,21,21], 1)
        plane_data = neural_data[pp][0]
        r_squares_slice.append(np.empty((np.shape(plane_data)[1], 2)))
        r_squares_no_laser.append(np.empty((np.shape(plane_data)[1], 2)))
        r_squares_no_at.append(np.empty((np.shape(plane_data)[1], 2)))

        coeffs_slice.append(np.empty((np.shape(plane_data)[1], len(predictor_names))))

        coeffs_lasso.append(np.empty((np.shape(plane_data)[1], len(predictor_names))))
        for nn in range(np.shape(plane_data)[1]):
            neuron = plane_data[:, nn]
            X_train, X_test, y_train, y_test = train_test_split(X, neuron, test_size=0.2, random_state=42)

            # Poisson test
            possion_predictors = predictor_names[:]
            possion_predictors.append('spks')
            spks = np.reshape(y_train, (len(y_train), -1))
            spks_test = np.reshape(y_test, (len(y_test), -1))

            scaler = StandardScaler()
            scaled_X = scaler.fit_transform(X_train)
            df_X = pd.DataFrame(np.concatenate((scaled_X, spks), axis=1), columns=possion_predictors)
            model = glm('spks ~ AT*V1 + AT*V2 + O1 + O2 + AT*LA + R1 + L1', data=df_X, family=sm.families.NegativeBinomial()).fit()

            scaled_test = scaler.fit_transform(X_test)
            df_test = pd.DataFrame(np.concatenate((scaled_test, spks_test), axis=1), columns=possion_predictors)
            # Display model results
            print(model.summary())
            #print(model.pearson_chi2/model.df_residual)

            spk_train_pred = model.predict(df_X)
            spk_test_pred = model.predict(df_test)


            plt.figure()
            plt.plot(spks)
            plt.plot(spk_train_pred)
            plt.show()

            plt.figure()
            plt.plot(y_test)
            plt.plot(spk_test_pred)
            plt.show()

            ridge_steps = [('imputation', SimpleImputer()),
                     ('scaler', StandardScaler()),
                     ('ridge', Ridge())]

            ridge_pipe = Pipeline(ridge_steps)

            alphas = {'ridge__alpha': np.logspace(-4, 0, 50)}

            gm_cv = GridSearchCV(ridge_pipe, alphas, cv=5)
            gm_cv.fit(X_train, y_train)

            r2 = gm_cv.score(X_test, y_test)
            print("Tuned ridge alpha: {}".format(gm_cv.best_params_))
            print("Best score while training: {}".format(gm_cv.best_score_))
            print("Tuned R squared on hold out data: {}".format(r2))

            r_squares_slice[pp][nn, 0] = gm_cv.best_score_
            r_squares_slice[pp][nn, 1] = r2


            coeffs = gm_cv.best_estimator_[2].coef_
            coeffs_slice[pp][nn, :] = coeffs

            for cc in zip(coeffs, predictor_names):
                #print('Tuned coefficients: {}'.format(gm_cv.best_estimator_[2].coef_))
                print(cc[1] + ': ' + str(cc[0]))

            # Repeat without laser
            no_laser_predictors = predictor_names[:]
            no_laser_predictors.remove('LA')

            X_nl = X[:]
            X_nl = np.delete(X_nl, 5, axis=1)
            X_train_nl, X_test_nl, y_train_nl, y_test_nl = train_test_split(X_nl, neuron, test_size=0.2, random_state=42)

            ridge_pipe_no_laser = Pipeline(ridge_steps)

            gm_cv_no_laser = GridSearchCV(ridge_pipe_no_laser, alphas, cv=5)
            gm_cv_no_laser.fit(X_train_nl, y_train_nl)

            r2_no_laser = gm_cv_no_laser.score(X_test_nl, y_test_nl)
            print("Tuned no laser ridge alpha: {}".format(gm_cv_no_laser.best_params_))
            print("Best score while training - no laser: {}".format(gm_cv_no_laser.best_score_))
            print("Tuned R squared on hold out data - no laser: {}".format(r2_no_laser))

            r_squares_no_laser[pp][nn, 0] = gm_cv_no_laser.best_score_
            r_squares_no_laser[pp][nn, 1] = r2_no_laser

            coeffs = gm_cv_no_laser.best_estimator_[2].coef_

            for cc in zip(coeffs, no_laser_predictors):
                # print('Tuned coefficients: {}'.format(gm_cv.best_estimator_[2].coef_))
                print(cc[1] + ': ' + str(cc[0]))

            # Repeat without attention
            no_att_predictors = predictor_names[:]
            no_att_predictors.remove('AT')

            X_at = X[:]
            X_at = np.delete(X_at, 4, axis=1)
            X_train_at, X_test_at, y_train_at, y_test_at = train_test_split(X_at, neuron, test_size=0.2,
                                                                                random_state=42)

            ridge_pipe_no_at = Pipeline(ridge_steps)

            gm_cv_no_at = GridSearchCV(ridge_pipe_no_at, alphas, cv=5)
            gm_cv_no_at.fit(X_train_at, y_train_at)

            r2_no_at = gm_cv_no_laser.score(X_test_at, y_test_at)
            print("Tuned no attention ridge alpha: {}".format(gm_cv_no_at.best_params_))
            print("Best score while training - no attention: {}".format(gm_cv_no_at.best_score_))
            print("Tuned R squared on hold out data - no attention: {}".format(r2_no_at))

            r_squares_no_at[pp][nn, 0] = gm_cv_no_at.best_score_
            r_squares_no_at[pp][nn, 1] = r2_no_at

            coeffs = gm_cv_no_at.best_estimator_[2].coef_

            for cc in zip(coeffs, no_att_predictors):
                # print('Tuned coefficients: {}'.format(gm_cv.best_estimator_[2].coef_))
                print(cc[1] + ': ' + str(cc[0]))

            # Repeat for lasso model
            lasso_steps = [('imputation', SimpleImputer()),
                           ('scaler', StandardScaler()),
                           ('lasso', Lasso())]

            lasso_pipe = Pipeline(lasso_steps)

            l_alphas = {'lasso__alpha': np.logspace(-4, 0, 50)}

            gm_cv_lasso = GridSearchCV(lasso_pipe, l_alphas, cv=5)
            gm_cv_lasso.fit(X_train, y_train)

            lasso_r2 = gm_cv.score(X_test, y_test)
            print("Tuned lasso alpha: {}".format(gm_cv_lasso.best_params_))
            print("Best score while training - lasso: {}".format(gm_cv_lasso.best_score_))
            print("Tuned R squared on hold out data - lasso: {}".format(lasso_r2))

            coeffs = gm_cv_lasso.best_estimator_[2].coef_
            coeffs_lasso[pp][nn, :] = coeffs

            for cc in zip(coeffs, predictor_names):
                # print('Tuned coefficients: {}'.format(gm_cv.best_estimator_[2].coef_))
                print(cc[1] + ': ' + str(cc[0]))

            y_pred_ridge = gm_cv.predict(X)
            y_pred_no_laser = gm_cv_no_laser.predict(X_nl)
            y_pred_lasso = gm_cv_lasso.predict(X)

            '''
            plt.figure()
            plt.plot(neuron)
            plt.plot(y_pred_ridge)
            plt.plot(y_pred_no_laser)
            plt.plot(y_pred_lasso)
            plt.legend(['Original', 'ridge', 'no laser', 'lasso'])
            plt.ylabel('dF/F')
            plt.title('Neuron: {}'.format(nn))
            plt.show()
            '''

    lasso_coeffs = np.concatenate(coeffs_lasso)
    df_lasso = pd.DataFrame(lasso_coeffs, columns=predictor_names)
    #plt.figure()
    #sns.boxplot(data=df_lasso)
    #plt.figure()
    #sns.violinplot(data=df_lasso)

    all_rsquares = np.concatenate(r_squares_slice)
    nl_rsquares = np.concatenate(r_squares_no_laser)
    test_diff = all_rsquares[:, 1] - nl_rsquares[:, 1]
    test_diff = np.reshape(test_diff, (len(test_diff), -1))

    at_rsquares = np.concatenate(r_squares_no_at)
    at_diff = all_rsquares[:, 1] - at_rsquares[:, 1]
    at_diff = np.reshape(at_diff, (len(at_diff), -1))

    all_coeffs = np.concatenate(coeffs_slice)
    all_data = np.concatenate((all_coeffs, all_rsquares, nl_rsquares, test_diff, at_rsquares, at_diff), axis=1)
    predictor_names.append('TrainR2')
    predictor_names.append('TestR2')
    predictor_names.append('nl_train_R2')
    predictor_names.append('nl_test_R2')
    predictor_names.append('nl_test_diff')
    predictor_names.append('at_train_R2')
    predictor_names.append('at_test_R2')
    predictor_names.append('at_test_diff')

    df = pd.DataFrame(all_data, columns=predictor_names)

    return df, df_lasso
    #sns.jointplot(data=df, x='TestR2', y='nl_test_diff')
    #sns.jointplot(data=df, x="TrainR2", y="TestR2")
    #sns.jointplot(data=df, x="TestR2", y="LA")
    #plt.figure()
    #sns.boxplot(data=df)
    #sns.violinplot(data=df)
    #sns.pairplot(df)


root = tk.Tk()
root.withdraw()

folder_path = filedialog.askdirectory()
f = []
for (dirpath, dirnames, filenames) in os.walk(folder_path):
    f.extend(filenames)

#session_path = r'W:\Data\Dylan\tdt_controls\Processed\20210307_161759__CTL001_B2_SWITCHOP\iMag\regression_data.mat'

df_list = []
df_lasso_list = []
for ii in range(len(filenames)):
    session_path = os.path.join(dirpath, filenames[ii])
    df, df_lasso = run_models(session_path)
    sns.jointplot(data=df, x='TestR2', y='nl_test_diff')
    df_list.append(df)
    df_lasso_list.append(df_lasso)