import sys
import os
import tkinter as tk
from tkinter import filedialog
import pandas as pd
import getopt
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge, TweedieRegressor, Lasso
from data_loading.import_PV_data import ParadoxData
import copy
import patsy
from third_regressions_the_charm import fit_model
import seaborn as sns


# In case I want to make it available to the terminal in good time
# if __name__ == "__main__":

model_kind = 'Ridge'

# Ask for the appropriate folder
root = tk.Tk()
root.withdraw()

vis1_coefs = []
vis1_features = []
vis2_coefs = []
vis2_features = []
sessions = []

folder_path = filedialog.askdirectory()
loaded_data = []
for (dirpath, dirnames, filenames) in os.walk(folder_path):
    for file in filenames:
        if 'paradox_response_data' not in file:
            continue

        imported_data = ParadoxData(os.path.join(dirpath, file))
        loaded_data.append(imported_data)
        sessions.append(imported_data.session)

#
        vis1_df = copy.copy(imported_data.base_df)
        vis1_df['no_laser'] = imported_data.vis1_mean[:, 0]

        column_names = list(vis1_df.columns)
        budding_formula = ''.join(['standardize(' + x + ') + ' for x in column_names if 'PV' not in x])
        categoricals = 'C(PV)'

        # # Testing the inclusion of the low power laser effect to see if that dominates
        # vis1_df['low_diff'] = imported_data.vis1_mean[:, 1] - imported_data.vis1_mean[:, 0]
        # budding_formula = budding_formula + "standardize(low_diff) + "

        vis1_df['laser_diff'] = imported_data.vis1_laser_diff
        full_formula = 'laser_diff ~ ' + budding_formula + categoricals

        # Run regression or vis 1 difference between low and high laser first
        activity, feature_data = patsy.dmatrices(full_formula, data=vis1_df, return_type='dataframe')
        r2, validated_model = fit_model(feature_data, activity, model_kind)
        coefficients = validated_model.best_estimator_.named_steps.ridge.coef_
        vis1_coefs.append(coefficients)
        feature_names = validated_model.best_estimator_.named_steps.ridge.feature_names_in_
        vis1_features.append(feature_names)


        #Run regression for vis 2
        vis2_df = copy.copy(imported_data.base_df)
        vis2_df['no_laser'] = imported_data.vis2_mean[:, 0]
        vis2_df['laser_diff'] = imported_data.vis2_laser_diff

        # # Testing the inclusion of the low power laser effect to see if that dominates
        # vis2_df['low_diff'] = imported_data.vis2_mean[:, 1] - imported_data.vis2_mean[:, 0]

        activity, feature_data = patsy.dmatrices(full_formula, data=vis2_df, return_type='dataframe')
        r2, validated_model = fit_model(feature_data, activity, model_kind)
        coefficients = validated_model.best_estimator_.named_steps.ridge.coef_
        vis2_coefs.append(coefficients)
        feature_names = validated_model.best_estimator_.named_steps.ridge.feature_names_in_
        vis2_features.append(feature_names)

vis1_output_df = pd.DataFrame(np.concatenate(vis1_coefs[:], axis=0), index=sessions, columns=vis1_features[0])

sns.heatmap(vis1_output_df, cmap="viridis")
plt.title('Vis 1 - low vs high diff')
plt.show()

vis2_output_df = pd.DataFrame(np.concatenate(vis2_coefs[:], axis=0), index=sessions, columns=vis2_features[0])
sns.heatmap(vis2_output_df, cmap="viridis")
plt.title('Vis 2 - low vs high diff')
plt.show()
