import os
import tkinter as tk
from tkinter import filedialog
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from data_loading.import_PV_data import ParadoxData
import copy
import patsy
from third_regressions_the_charm import fit_model
import seaborn as sns

# In case I want to make it available to the terminal in good time
# if __name__ == "__main__":

def run_regression_one_laser_diff(pv_data, laser):
    #
    vis1_df = copy.copy(pv_data.base_df)

    # TODO: consider removing no laser for this regression
    # vis1_df['no_laser'] = pv_data.vis1_mean[:, 0]

    column_names = list(vis1_df.columns)
    budding_formula = ''.join(['standardize(' + x + ') + ' for x in column_names if 'PV' not in x])
    categoricals = 'C(PV)'

    vis1_df['laser_diff'] = pv_data.vis1_mean[:, laser] - pv_data.vis1_mean[:, 0]
    full_formula = 'laser_diff ~ ' + budding_formula + categoricals

    # Run regression or vis 1 difference between low and high laser first
    activity, feature_data = patsy.dmatrices(full_formula, data=vis1_df, return_type='dataframe')
    r2, validated_model = fit_model(feature_data, activity, model_kind)
    vis1_coefficients = validated_model.best_estimator_.named_steps.ridge.coef_
    vis1_features = validated_model.best_estimator_.named_steps.ridge.feature_names_in_

    # Run regression for vis 2
    vis2_df = copy.copy(pv_data.base_df)
    # vis2_df['no_laser'] = pv_data.vis2_mean[:, 0]
    vis2_df['laser_diff'] = pv_data.vis2_laser_diff

    activity, feature_data = patsy.dmatrices(full_formula, data=vis2_df, return_type='dataframe')
    r2, validated_model = fit_model(feature_data, activity, model_kind)
    vis2_coefficients = validated_model.best_estimator_.named_steps.ridge.coef_
    vis2_features = validated_model.best_estimator_.named_steps.ridge.feature_names_in_

    return vis1_coefficients, vis1_features, vis2_coefficients, vis2_features

model_kind = 'Ridge'

# Ask for the appropriate folder
root = tk.Tk()
root.withdraw()

model_results = {'low': {'vis1': {'coef': [], 'feat': []}, 'vis2': {'coef': [], 'feat': []}},
                 'high': {'vis1': {'coef': [], 'feat': []}, 'vis2': {'coef': [], 'feat': []}}}
sessions = []

folder_path = filedialog.askdirectory()
loaded_data = []
for (dirpath, dirnames, filenames) in os.walk(folder_path):
    for file in filenames:
        if 'paradox_response_data' not in file:
            continue

        imported_data = ParadoxData(os.path.join(dirpath, file))
        loaded_data.append(imported_data)
        sessions.append(imported_data.session)

        # Todo: write this in a better way in the future - surely there is a dry-er way than this
        coefs_1_low, feat_1_low, coef_2_low, feat_2_low = run_regression_one_laser_diff(imported_data, laser=1)
        model_results['low']['vis1']['coef'].append(coefs_1_low)
        model_results['low']['vis1']['feat'].append(feat_1_low)
        model_results['low']['vis2']['coef'].append(coef_2_low)
        model_results['low']['vis2']['feat'].append(feat_2_low)

        coefs_1_high, feat_1_high, coef_2_high, feat_2_high = run_regression_one_laser_diff(imported_data, laser=2)
        model_results['high']['vis1']['coef'].append(coefs_1_high)
        model_results['high']['vis1']['feat'].append(feat_1_high)
        model_results['high']['vis2']['coef'].append(coef_2_high)
        model_results['high']['vis2']['feat'].append(feat_2_high)

# looping through laser powers
for label, power_data in model_results.items():
    # looping through visual stimuli
    for stimulus, model_data in model_results[label].items():
        results_df = pd.DataFrame(np.concatenate(model_data['coef'][:], axis=0), index=sessions, columns=model_data['feat'][0])

        sns.heatmap(results_df, cmap="viridis")
        plt.title(label + ' laser power, ' + stimulus)
        plt.show()
