import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sklearn.decomposition as skl


def run_model(neural_data, score_components=True, max_components=50, model_choice='FA'):

    if score_components:
        fa_score = np.empty(max_components)

        for num_of_components in range(max_components):
            if model_choice == 'FA':
                fa_model = skl.FactorAnalysis(n_components=num_of_components + 1).fit(reshaped_data)
                fa_score[num_of_components] = fa_model.score(reshaped_data)
            elif model_choice == 'PCA':
                fa_model = skl.PCA(n_components=num_of_components + 1).fit(reshaped_data)
                fa_score[num_of_components] = fa_model.score(reshaped_data)

        fig = plt.figure()
        plt.plot(range(max_components), fa_score)
        plt.show()

    if model_choice == 'FA':
        print('Data reshaped, running FA.')
        fa_model = skl.FactorAnalysis(n_components=12).fit(reshaped_data)
    elif model_choice == 'PCA':
        print('Data reshaped, running PCA.')
        fa_model = skl.PCA(n_components=12).fit(reshaped_data)

    print('Transforming data.')
    transformed_data = fa_model.transform(reshaped_data)

    reshaped_transform = np.empty((num_of_trials, 12, num_of_timepoints))
    for dim in range(12):
        reshaped_transform[:, dim, :] = np.ndarray.reshape(transformed_data[:, dim], (num_of_trials, num_of_timepoints))

    return reshaped_transform, model_choice
