from sklearn import svm
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsClassifier
from data_preparation import load_data, get_vis_trials, reshape_data, normalise_data
from make_it_smaller import run_model
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
import numpy as np
from scipy import stats
import os
import tkinter as tk
from tkinter import filedialog
from data_loading.session_data import MatLabData, SimplifiedMatLabData
from scipy import stats
from statsmodels.stats.multitest import multipletests

"""Decode conditions from imported Spyglass data

Uses Scikit-learn classifiers to decode the trial condition from imported MatLab class. 

TODO: Make the code more flexible so that it can decode arbitrary conditions on an arbitrary selection of conditions. 

TODO: Allow training and testing of the same decoder on different subsets of data. 

TODO: flesh out documentation of subfunctions. 

TODO: plot the output of the repeated results for the balanced splits with confidence intervals.
TODO: decide how many repetitions with the label balancing is acceptable. 
    Is there a measure which would be most resistant to an inflated sample size? 

TODO: check MatLab data import works and add sample_trials which selects a subset of trials as a method to that class. 

TODO: Generalise balance_labels to more than 2 groups. 
"""


def run_decoder(data, labels, method='Logistic', test_only_data=None, test_only_labels=None):
    """Gets cross-validated scores for the chosen classifier on both the training data and out of sample data."""
    scores = []
    test_only_scores = []

    coefficients = np.empty((5, np.shape(data)[1]))

    class_dict = {
        'Logistic': LogisticRegression(max_iter=1000),
        'SVM': svm.SVC(kernel='linear'),
        'rbfSVM': svm.SVC(kernel='rbf'),
        'Nearest': KNeighborsClassifier(3)
    }

    classifier = class_dict[method]

    skf = StratifiedKFold()
    counter = 0
    for train_index, test_index in skf.split(data, labels):
        data_train, data_test = data[train_index], data[test_index]
        labels_train, labels_test = labels[train_index], labels[test_index]

        classifier.fit(data_train, labels_train)
        score = classifier.score(data_test, labels_test)
        scores.append(score)

        if test_only_data is not None:
            individual_score = classifier.score(test_only_data, test_only_labels)
            test_only_scores.append(individual_score)

        # Todo: maybe permanently remove this bit of code
        # coefficients[counter, :] = classifier.coef_[0]
        # counter = counter + 1
        # average_coefs = np.average(coefficients, 0)
        # classifier.coef_[0] = average_coefs

    if test_only_data is None:
        return classifier, scores
    else:
        return classifier, scores, test_only_scores


def repeat_across_time(base_data, base_labels, method="Nearest", test_only_data=None, test_only_labels=None):
    """Train the decoder on the non-laser trials - one time point at a time

    Time is assumed to be the 3rd dimension - could be changed later
    """
    test_scores = []
    average_scores = []
    sem_scores = []
    out_of_sample_scores = []
    sem_oos_scores = []
    for time_column in range(np.shape(base_data)[2]):
        time_sample = base_data[:, :, time_column]

        if test_only_data is None:
            average_clf, scores = run_decoder(time_sample, base_labels, method)
        else:
            test_time_sample = test_only_data[:, :, time_column]
            average_clf, scores, individual_scores = run_decoder(time_sample, base_labels, method, test_time_sample,
                                                                 test_only_labels)

            # CAUTION: In retrospect I don't think that averaging coefficients this way is sensible at all.
            # test_score = average_clf.score(test_time_sample, test_only_labels)
            # test_scores.append(test_score)
            # When the test data is run on each decoder in turn and the collective result is pooled.
            out_of_sample_scores.append(np.average(individual_scores))
            sem_oos_scores.append(stats.sem(individual_scores))

        average_scores.append(np.average(scores))
        sem_scores.append(stats.sem(scores))

    if test_only_data is None:
        return average_scores, sem_scores
    else:
        return average_scores, sem_scores, out_of_sample_scores, sem_oos_scores


def iterate_balanced_decoder(training_data, training_labels, testing_data, testing_labels, repeat_number=20,
                             show_iteration_plots=False, method="Nearest"):
    """
    Repeatedly runs the selected classifiers across the trials time using a balanced selection of trial types.
    The aim is to train and test the classifier with the same number of trials of each type to avoid introducing bias,
    whilst repeating the procedure a sufficient number of times that any error introduced by the sampling is evened out.
    """

    trained_data_scores = np.empty((repeat_number, np.shape(training_data)[2]))
    # all_laser_test_scores = np.empty((repeat_number, np.shape(training_data)[2]))
    out_of_sample_data_scores = np.empty((repeat_number, np.shape(training_data)[2]))
    for rep in range(repeat_number):

        # Balance the non-laser trials
        balance_index = balance_labels(training_labels)

        balanced_trials = training_labels[balance_index]
        balanced_sample = training_data[balance_index, :, :]

        average_scores, sem_scores, test_only_scores, sem_test_only = repeat_across_time(balanced_sample,
                                                                                         balanced_trials, method,
                                                                                         test_only_data=testing_data,
                                                                                         test_only_labels=testing_labels)

        if show_iteration_plots:
            # Plot the results
            fig = plt.figure()
            plt.errorbar(x_data_time, average_scores, yerr=sem_scores)
            plt.errorbar(x_data_time, test_only_scores, yerr=sem_test_only)
            # plt.plot(x_data_time, laser_scores)
            # plt.legend(['Laser', 'Laser ind', 'Non-laser'])
            plt.legend(['Trained data', 'Out of sample'])
            plt.ylabel('Decoder performance')
            plt.xlabel('Time (s)')
            plt.title('Iteration: ' + str(rep))
            plt.show()

        trained_data_scores[rep, :] = average_scores
        # all_laser_test_scores[rep, :] = laser_scores
        out_of_sample_data_scores[rep, :] = test_only_scores

    return trained_data_scores, out_of_sample_data_scores


def balance_labels(labels):
    """
    Function to fetch the same number of trials of each condition from the list provided.
    """

    unique_labels, counts = np.unique(labels, return_counts=True)
    largest_group_size = np.max(counts)
    smallest_group_size = np.min(counts)
    smallest_group = np.where(counts == smallest_group_size)[0][0]
    largest_group = np.where(counts == largest_group_size)[0][0]

    trials_to_exclude = largest_group_size - smallest_group_size
    balance_index = np.tile(True, np.shape(labels))
    large_group_index = np.where(labels == unique_labels[largest_group])[0]
    if trials_to_exclude == 0:
        return balance_index
    else:
        balance_index[np.random.permutation(large_group_index)[0:trials_to_exclude]] = False

    return balance_index


def sample_trials(model_data, filter_labels, chosen_trials, output_labels=None):
    for ii in range(np.shape(chosen_trials)[0]):
        if ii == 0:
            trial_index = filter_labels == chosen_trials[ii]
        else:
            trial_index = np.logical_or(trial_index, filter_labels == chosen_trials[ii])

    if output_labels is None:
        filtered_output_labels = filter_labels[trial_index]
    else:
        filtered_output_labels = output_labels[trial_index]

    filtered_model_data = model_data[trial_index, :, :]
    unused_labels, trial_counts = np.unique(filtered_output_labels, return_counts=True)

    return filtered_model_data, filtered_output_labels, trial_counts


def basic_plot(time_data, non_laser, laser, title_tag, plot_save_path, pVal='', pval_list=[]):
    # Plot the results
    fig = plt.figure()

    # plt.errorbar(x_data_time, np.average(vis1_non_laser_scores, 0), yerr=stats.sem(vis1_non_laser_scores, 0))
    # #plt.errorbar(x_data_time, np.average(vis1_laser_ind, 0), yerr=stats.sem(vis1_laser_ind, 0))
    # plt.errorbar(x_data_time, np.average(vis1_laser_scores, 0), yerr=stats.sem(vis1_laser_scores, 0))

    plt.errorbar(time_data, np.average(non_laser, 0), yerr=np.std(non_laser, 0))
    plt.errorbar(time_data, np.average(laser, 0), yerr=np.std(laser, 0))
    if len(pval_list) != 0:
        for tt in range(len(time_data)):
            plt.text(time_data[tt], 0.90, pval_list[tt])

    plt.legend(['Attend', 'Ignore'], loc='upper left')
    plt.ylabel('Decoder performance')
    plt.xlabel('Time (s)')
    plt.title(title_tag + ' ' + str(pVal) + ' - Average performance')
    save_p = plot_save_path + r'\average_decoding_performance_' + title_tag
    fig.savefig(save_p)

def convert_to_sig_tags(pvals):
    sig_tags = []
    for ss in range(len(pvals)):
        if pvals[ss] < 0.001:
            sig_tags.append('***')
        elif pvals[ss] < 0.01:
            sig_tags.append('**')
        elif pvals[ss] < 0.05:
            sig_tags.append('*')
        else:
            sig_tags.append('n.s')

    return sig_tags


if __name__ == '__main__':

     # 'Logistic','SVM','rbfSVM','Nearest'
    class_method = 'Logistic'

    root = tk.Tk()
    root.withdraw()

    folder_path = filedialog.askdirectory()
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)
        break

    # Check which level we're at:
    contents_test = []
    for ii in range(len(f)):
        contents_test.append('imgtrl_base_data' in f[ii])

    dir1_contents_test = []
    # d = []
    for dd in range(len(dirnames)):
        data_present = []
        for (subdirpath, subdirnames, subfilenames) in os.walk(os.path.join(dirpath, dirnames[dd])):
            # d.extend(subfilenames)
            if len(subfilenames) != 0:
                data_present.append('imgtrl_base_data' in subfilenames[0])
        dir1_contents_test.append(any(data_present))

    # dir1_contents_test = []
    # for ii in range(len(d)):
    #     dir1_contents_test.append('imgtrl_base_data' in d[ii])

    loaded_data = []
    if any(contents_test):
        loaded_data.append(MatLabData(folder_path))
    elif any(dir1_contents_test):
        for jj in range(len(dirnames)):
            # a = SimplifiedMatLabData(os.path.join(dirpath, dirnames[jj]), model_choice='NMF')
            if dir1_contents_test[jj]:
                origin_path = os.path.join(dirpath, dirnames[jj])
                loaded_data.append(MatLabData(origin_path))

    # Perform dimensionality reduction

    # Filter for the desired trial types
    # - Classify laser power in attend condition
    # - Test in alternative condition
    # - Try both ways

    # Plot results of the decoding
    nl_train_nl_group = []
    nl_train_las_group = []
    las_train_nl_group = []
    las_train_las_group = []

    for kk in range(len(loaded_data)):
        save_path = os.path.join(loaded_data[kk].matlab_data_path, 'decoding_vis_stim', class_method)
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        # NMF_path = save_path + r'\NMF_model.npy'

        # Filter it for only the visual trials
        filtered_data, visual_trials, time_idx = get_vis_trials(loaded_data[kk].trial_data, loaded_data[kk].bhv,
                                                                loaded_data[kk].trial_time)

        # Remove VIP cells
        non_vip_index = [x[0] != 'VIP' for x in loaded_data[kk].cell_label]
        filtered_data = filtered_data[:, non_vip_index, :]

        x_data_time = loaded_data[kk].trial_time[time_idx[0]:time_idx[1]]

        num_of_trials = np.shape(filtered_data)[0]
        num_of_neurons = np.shape(filtered_data)[1]
        num_of_time_points = np.shape(filtered_data)[2]

        # Reshapes the array so that it is 2D rather than 3D
        columnar_data = reshape_data(filtered_data)

        # Mean normalise and scale by variance
        scaled_data = normalise_data(columnar_data)

        # path_good = os.path.isfile(NMF_path)
        # if path_good:
        #     temp_load = np.load(NMF_path, allow_pickle=True)
        #     NMF_data = temp_load[0]
        #     dr_model = temp_load[1]
        # else:
        #     # Run dimensionality reduction on the dataset
        #     print('Running NMF model')
        #     chosen_num_of_dims = 20
        #     NMF_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points,
        #                                                chosen_num_of_dims,
        #                                                'NMF')
        #     np.save(NMF_path, [NMF_data, dr_model])
        # pca = PCA(n_components='mle', svd_solver='full')
        # pca_data = pca.fit_transform(scaled_data)
        # Consider playing around with other settings for PCA to constrain the component number, e.g. 'mle'
        # Todo: come up with a better solution.
        scaled_data[np.isnan(scaled_data)] = 0

        NMF_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points, 20, 'PCA')

        sel = loaded_data[kk].bhv['sel']
        # Separate off the non-laser trials - split rel/irrel vis1/vis2 - can also try multi-output.
        all_condition_labels = sel['condAll'][visual_trials]
        coarse_condition_labels = sel['condCoarse'][visual_trials]

        ori_combi = sel['combi'][sel['combi2ori'][:, 0] == 165, :]
        attend_conds = ori_combi[0, :]
        ignore_conds = ori_combi[1, :]

        # Vis 1 decoding
        # Get just the vis1 and vis2 attend
        no_laser_att_sample, no_laser_att_trials, non_laser_att_counts = sample_trials(NMF_data, all_condition_labels, attend_conds.flatten(),
                                                                                       coarse_condition_labels)

        # Need to flatten trial types so that they all attend and all ignore have the same value
        # pool_no_laser_att_trials = np.copy(no_laser_att_trials)
        # pool_no_laser_att_trials[no_laser_att_trials == 2] = 1
        # pool_no_laser_att_trials[no_laser_att_trials == 4] = 3
        # pool_non_laser_counts = [np.sum(non_laser_att_counts[:2]), np.sum(non_laser_att_counts[2:])]


        print(str(non_laser_att_counts[0]) + ' vis1 attend trials, ' + str(
            non_laser_att_counts[1]) + ' vis2 attend trials.')

        # Get just the vis1 and vis2 ignore trials
        laser_att_sample, laser_att_trials, laser_att_counts = sample_trials(NMF_data, all_condition_labels, ignore_conds.flatten(),
                                                                             coarse_condition_labels)

        # pool_laser_att_trials = np.copy(laser_att_trials)
        laser_att_trials[laser_att_trials == 3] = 1
        laser_att_trials[laser_att_trials == 4] = 2
        # pool_laser_counts = [np.sum(laser_att_counts[:2]), np.sum(laser_att_counts[2:])]


        print(str(laser_att_counts[0]) + ' vis1 ignore trials, ' + str(laser_att_counts[1]) + ' vis2 ignore trials.')

        print('Decoding vis stim trained on attend')
        nl_train_nl_scores, nl_trail_las_scores = iterate_balanced_decoder(no_laser_att_sample,
                                                                                          no_laser_att_trials,
                                                                                          laser_att_sample, laser_att_trials, method=class_method)

        print('Decoding vis stim trained on ignore')
        las_train_las_scores, las_trail_nl_scores = iterate_balanced_decoder(laser_att_sample, laser_att_trials, no_laser_att_sample,
                                                                                        no_laser_att_trials, method=class_method)

        print('Plotting and saving the results')
        basic_plot(x_data_time, nl_train_nl_scores, nl_trail_las_scores, 'attend train', save_path)
        basic_plot(x_data_time, las_trail_nl_scores, las_train_las_scores, 'ignore train', save_path)

        if num_of_time_points > 13:
            downsample_Idx = np.round(np.linspace(0, num_of_time_points-1, num=13))
            downsample_Idx = downsample_Idx.astype(int)
            nl_train_nl_scores = nl_train_nl_scores[:,downsample_Idx]
            nl_trail_las_scores = nl_trail_las_scores[:,downsample_Idx]
            las_trail_nl_scores = las_trail_nl_scores[:,downsample_Idx]
            las_train_las_scores = las_train_las_scores[:,downsample_Idx]

        nl_train_nl_group.append(np.nanmean(nl_train_nl_scores, axis=0))
        nl_train_las_group.append(np.nanmean(nl_trail_las_scores, axis=0))
        las_train_nl_group.append(np.nanmean(las_trail_nl_scores, axis=0))
        las_train_las_group.append(np.nanmean(las_train_las_scores, axis=0))


    # Concatenate the data into arrays and then make the same basic plots.
    nl_nl_array = np.array(nl_train_nl_group)
    nl_las_array = np.array(nl_train_las_group)
    las_nl_array = np.array(las_train_nl_group)
    las_las_array = np.array(las_train_las_group)

    nl_nl_avg = np.nanmean(nl_nl_array[:, np.logical_and(x_data_time>=0, x_data_time<=1.5)], axis=1)
    nl_las_avg = np.nanmean(nl_las_array[:, np.logical_and(x_data_time>=0, x_data_time<=1.5)], axis=1)
    las_nl_avg = np.nanmean(las_nl_array[:, np.logical_and(x_data_time>=0, x_data_time<=1.5)], axis=1)
    las_las_avg = np.nanmean(las_las_array[:, np.logical_and(x_data_time>=0, x_data_time<=1.5)], axis=1)

    nl_pvals = []
    las_pvals = []
    train_pvals = []
    for tt in range(np.shape(nl_nl_array)[1]):
        [temp_nl_stat, temp_nl_pval] = stats.wilcoxon(nl_nl_array[:, tt], nl_las_array[:, tt])
        nl_pvals.append(temp_nl_pval)
        [temp_las_stat, temp_las_pval] = stats.wilcoxon(las_nl_array[:, tt], las_las_array[:, tt])
        las_pvals.append(temp_las_pval)
        [temp_train_stat, temp_train_pval] = stats.wilcoxon(nl_nl_array[:, tt], las_las_array[:, tt])
        train_pvals.append(temp_train_pval)


    shorter_t_idx = x_data_time <= 1
    reject, nl_corrected_pvals, alphacSidak, alphacbon = multipletests(nl_pvals, method='bonferroni')
    reject, las_corrected_pvals, alphacSidak, alphacbon = multipletests(las_pvals, method='bonferroni')
    reject, train_corrected_pvals, alphacSidak, alphacbon = multipletests(las_pvals, method='bonferroni')

    # Shorter pvals
    nl_pvals_short = np.array(nl_pvals)[shorter_t_idx]
    las_pvals_short = np.array(las_pvals)[shorter_t_idx]
    reject, nl_corrected_pvals_short, alphacSidak, alphacbon = multipletests(nl_pvals_short, method='bonferroni')
    reject, las_corrected_pvals_short, alphacSidak, alphacbon = multipletests(las_pvals_short, method='bonferroni')

    nl_sig = convert_to_sig_tags(nl_corrected_pvals)
    las_sig = convert_to_sig_tags(las_corrected_pvals)
    train_sig = convert_to_sig_tags(train_corrected_pvals)
    nl_sig_short = convert_to_sig_tags(nl_corrected_pvals_short)
    las_sig_short = convert_to_sig_tags(las_corrected_pvals_short)

    group_save_path = os.path.join(folder_path, 'decoding_vis_stim', class_method)
    if not os.path.isdir(group_save_path):
        os.makedirs(group_save_path)


    [nlt_stat, nlt_pval] = stats.wilcoxon(nl_nl_avg, nl_las_avg)
    basic_plot(x_data_time, nl_nl_array, nl_las_array, 'Attend train', group_save_path, pVal=nlt_pval, pval_list=nl_sig)
    [las_stat, las_pval] = stats.wilcoxon(las_nl_avg, las_las_avg)
    basic_plot(x_data_time, las_nl_array, las_las_array, 'Ignore train', group_save_path, pVal=las_pval, pval_list=las_sig)

    [train_stat, train_pval] = stats.wilcoxon(nl_nl_avg, las_las_avg)
    basic_plot(x_data_time, nl_nl_array, las_las_array, 'Self train comparison', group_save_path, pVal=train_pval,
               pval_list=train_sig)

    # Plot the same data again but with the shorter window
    nl_nl_avg_short = np.nanmean(nl_nl_array[:, shorter_t_idx], axis=1)
    nl_las_avg_short = np.nanmean(nl_las_array[:, shorter_t_idx], axis=1)
    las_nl_avg_short = np.nanmean(las_nl_array[:, shorter_t_idx], axis=1)
    las_las_avg_short = np.nanmean(las_las_array[:, shorter_t_idx], axis=1)

    [nlt_stat, nlt_pval_short] = stats.wilcoxon(nl_nl_avg_short, nl_las_avg_short)
    basic_plot(x_data_time[shorter_t_idx], nl_nl_array[:, shorter_t_idx], nl_las_array[:, shorter_t_idx], 'Attend train - short', group_save_path, pVal=nlt_pval_short,
               pval_list=nl_sig_short)
    [las_stat, las_pval_short] = stats.wilcoxon(las_nl_avg_short, las_las_avg_short)
    basic_plot(x_data_time[shorter_t_idx], las_nl_array[:, shorter_t_idx], las_las_array[:, shorter_t_idx], 'Attend train - short', group_save_path, pVal=las_pval_short,
               pval_list=las_sig_short)

# print(att_no_laser_group)
# min_group = []
# for sess in range(len(att_no_laser_group)):
#     min_group.append(att_no_laser_group[sess].size)
#
# group_length = min(min_group)
#
# vis1_no_laser_average = []
# vis1_laser_average = []
# vis2_no_laser_average = []
# vis2_laser_average = []
# for sess in range(len(att_no_laser_group)):
#     vis1_no_laser_average = np.append(vis1_no_laser_average, att_no_laser_group[sess][
#         np.round(np.linspace(0, att_no_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)
#     vis1_laser_average = np.append(vis1_laser_average, att_laser_group[sess][
#         np.round(np.linspace(0, att_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)
#     vis2_no_laser_average = np.append(vis2_no_laser_average, ign_no_laser_group[sess][
#         np.round(np.linspace(0, ign_no_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)
#     vis2_laser_average = np.append(vis2_laser_average, ign_laser_group[sess][
#         np.round(np.linspace(0, ign_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)


# for mm in range(len(vis1_no_laser_group)):

# # Try to decode laser from non-laser
#
# rel_trials = np.logical_or(all_condition_labels == 1, all_condition_labels == 3)
# rel_labels = all_condition_labels[rel_trials]
# typed_sample = NMF_data[rel_trials, :, :]
#
# # Balance the non-laser trials
# print(str(sum(rel_labels == 1)) + ' no laser vis1 trials, ' + str(
#     sum(rel_labels == 3)) + ' laser vis1 trials.')
# balance_index = balance_labels(rel_labels)
#
# balanced_trials = rel_labels[balance_index]
# balanced_sample = typed_sample[balance_index, :, :]
#
# # Train the decoder on the non-laser trials - one time point at a time
# average_scores = []
# sem_scores = []
# for time_column in range(np.shape(balanced_sample)[2]):
#     time_sample = balanced_sample[:, :, time_column]
#
#     # Test the decoder on laser and non-laser trials
#     average_clf, scores = run_decoder(time_sample, balanced_trials, method="logistic regression")
#     average_scores.append(np.average(scores))
#     sem_scores.append(stats.sem(scores))
#
# # Plot the results
# fig = plt.figure()
# plt.errorbar(x_data_time, average_scores, yerr=sem_scores)
# plt.legend(['Rel trials'])
# plt.ylabel('Decoder performance')
# plt.xlabel('Time (s)')
# plt.show()

#
# # Loop through dimension numbers and get laser decoder performance
# print('Calculating laser decoding performance for different dimensionalities...')
# rel_vis1_trials = np.logical_or(all_condition_labels == 1, all_condition_labels == 3)
# rel_vis1_labels = all_condition_labels[rel_vis1_trials]
#
# rel_vis2_trials = np.logical_or(all_condition_labels == 4, all_condition_labels == 6)
# rel_vis2_labels = all_condition_labels[rel_vis2_trials]
#
# irrel_vis1_trials = np.logical_or(all_condition_labels == 7, all_condition_labels == 9)
# irrel_vis1_labels = all_condition_labels[irrel_vis1_trials]
#
# irrel_vis2_trials = np.logical_or(all_condition_labels == 10, all_condition_labels == 12)
# irrel_vis2_labels = all_condition_labels[irrel_vis2_trials]
#
# r_vis1_scores = []
# r_vis2_scores = []
# i_vis1_scores = []
# i_vis2_scores = []
# r_vis1_sem = []
# r_vis2_sem = []
# i_vis1_sem = []
# i_vis2_sem = []
# NMF_models = []
# NMF_model_data = []
# NMF_path = save_path + r'\NMF_dimension_test.npy'
# for dim_number in np.linspace(1, 50, 50):
#     print('NNMF with dimensionality :' + str(int(dim_number)))
#     NMF_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points, int(dim_number),
#                                                'NMF')
#     NMF_models.append(dr_model)
#     NMF_model_data.append(NMF_data)
#     # Calculate for vis1 attending
#     typed_sample = NMF_data[rel_vis1_trials, :, :]
#     balance_index = balance_labels(rel_vis1_labels)
#     balanced_sample = typed_sample[balance_index, :, :]
#     balanced_trials = rel_vis1_labels[balance_index]
#
#     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
#     r_vis1_scores.append(np.average(average_scores))
#     r_vis1_sem.append(stats.sem(average_scores))
#
#     # Calculate for vis2 attending
#     typed_sample = NMF_data[rel_vis2_trials, :, :]
#     balance_index = balance_labels(rel_vis2_labels)
#     balanced_sample = typed_sample[balance_index, :, :]
#     balanced_trials = rel_vis2_labels[balance_index]
#
#     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
#     r_vis2_scores.append(np.average(average_scores))
#     r_vis2_sem.append(stats.sem(average_scores))
#
#     # Calculate for vis2 ignore
#     typed_sample = NMF_data[irrel_vis1_trials, :, :]
#     balance_index = balance_labels(irrel_vis1_labels)
#     balanced_sample = typed_sample[balance_index, :, :]
#     balanced_trials = irrel_vis1_labels[balance_index]
#
#     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
#     i_vis1_scores.append(np.average(average_scores))
#     i_vis1_sem.append(stats.sem(average_scores))
#
#     # Calculate for vis1 attending
#     typed_sample = NMF_data[irrel_vis2_trials, :, :]
#     balance_index = balance_labels(irrel_vis2_labels)
#     balanced_sample = typed_sample[balance_index, :, :]
#     balanced_trials = irrel_vis2_labels[balance_index]
#
#     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
#     i_vis2_scores.append(np.average(average_scores))
#     i_vis2_sem.append(stats.sem(average_scores))
#
# np.save(NMF_path, [NMF_model_data, NMF_models])
# lowerY = np.empty((2, 1))
# upperY = np.empty((2, 1))
# fig = plt.figure()
# ax1 = plt.subplot(2, 1, 1)
# rv1 = plt.errorbar(np.linspace(1, 50, 50), r_vis1_scores, yerr=r_vis1_sem)
# iv1 = plt.errorbar(np.linspace(1, 50, 50), i_vis1_scores, yerr=i_vis1_sem)
# lowerY[0], upperY[0] = plt.ylim()
# plt.hlines(0.5, 0, 50, linestyles='dotted')
# plt.xticks(np.arange(1, 51, step=1))
# plt.legend((rv1, iv1), ('Rel vis1', 'Irrel vis1'))
# plt.ylabel('Average decoder performance')
# plt.xlabel('Dimensions of NMF')
# plt.title('Decoding laser from non-laser')
#
# ax2 = plt.subplot(2, 1, 2)
# rv2 = plt.errorbar(np.linspace(1, 50, 50), r_vis2_scores, yerr=r_vis2_sem)
# iv2 = plt.errorbar(np.linspace(1, 50, 50), i_vis2_scores, yerr=i_vis2_sem)
# lowerY[1], upperY[1] = plt.ylim()
# plt.hlines(0.5, 0, 50, linestyles='dotted')
# plt.xticks(np.arange(1, 51, step=1))
# plt.legend((rv2, iv2), ('Rel vis2', 'Irrel vis2'))
# plt.ylabel('Average decoder performance')
# plt.xlabel('Dimensions of NMF')
# plt.title('Decoding laser from non-laser')
#
# axisLimits = [lowerY.min(), upperY.max()]
# ax1.set_ylim(axisLimits)
#
# save_p = save_path + r'\laser_decoding_across_dims'
# fig.savefig(save_p)
# plt.show()
