from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from data_preparation import load_data, get_vis_trials, reshape_data, normalise_data
from make_it_smaller import run_model
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
import numpy as np
from scipy import stats
import os
import tkinter as tk
from tkinter import filedialog
from data_loading.session_data import MatLabData, SimplifiedMatLabData

"""Decode conditions from imported Spyglass data

Uses Scikit-learn classifiers to decode the trial condition from imported MatLab class. 

TODO: Make the code more flexible so that it can decode arbitrary conditions on an arbitrary selection of conditions. 

TODO: Allow training and testing of the same decoder on different subsets of data. 

TODO: flesh out documentation of subfunctions. 

TODO: plot the output of the repeated results for the balanced splits with confidence intervals.
TODO: decide how many repetitions with the label balancing is acceptable. 
    Is there a measure which would be most resistant to an inflated sample size? 
    
TODO: check MatLab data import works and add sample_trials which selects a subset of trials as a method to that class. 

TODO: Generalise balance_labels to more than 2 groups. 
"""


def run_decoder(data, labels, method='Logistic', test_only_data=None, test_only_labels=None):
    """Gets cross-validated scores for the chosen classifier on both the training data and out of sample data."""
    scores = []
    test_only_scores = []

    coefficients = np.empty((5, np.shape(data)[1]))

    class_dict = {
        'Logistic': LogisticRegression(),
        'SVM': svm.SVC(kernel='linear'),
        'Nearest': KNeighborsClassifier(3)
    }

    classifier = class_dict[method]

    skf = StratifiedKFold()
    counter = 0
    for train_index, test_index in skf.split(data, labels):
        data_train, data_test = data[train_index], data[test_index]
        labels_train, labels_test = labels[train_index], labels[test_index]

        classifier.fit(data_train, labels_train)
        score = classifier.score(data_test, labels_test)
        scores.append(score)

        if test_only_data is not None:
            individual_score = classifier.score(test_only_data, test_only_labels)
            test_only_scores.append(individual_score)

        coefficients[counter, :] = classifier.coef_[0]
        counter = counter + 1

        average_coefs = np.average(coefficients, 0)
        classifier.coef_[0] = average_coefs

    if test_only_data is None:
        return classifier, scores
    else:
        return classifier, scores, test_only_scores


def repeat_across_time(base_data, base_labels, method="Logistic", test_only_data=None, test_only_labels=None):
    """Train the decoder on the non-laser trials - one time point at a time

    Time is assumed to be the 3rd dimension - could be changed later
    """
    test_scores = []
    average_scores = []
    sem_scores = []
    out_of_sample_scores = []
    sem_oos_scores = []
    for time_column in range(np.shape(base_data)[2]):
        time_sample = base_data[:, :, time_column]

        if test_only_data is None:
            average_clf, scores = run_decoder(time_sample, base_labels, method)
        else:
            test_time_sample = test_only_data[:, :, time_column]
            average_clf, scores, individual_scores = run_decoder(time_sample, base_labels, method, test_time_sample,
                                                                 test_only_labels)

            # CAUTION: In retrospect I don't think that averaging coefficients this way is sensible at all.
            # test_score = average_clf.score(test_time_sample, test_only_labels)
            # test_scores.append(test_score)
            # When the test data is run on each decoder in turn and the collective result is pooled.
            out_of_sample_scores.append(np.average(individual_scores))
            sem_oos_scores.append(stats.sem(individual_scores))

        average_scores.append(np.average(scores))
        sem_scores.append(stats.sem(scores))

    if test_only_data is None:
        return average_scores, sem_scores
    else:
        return average_scores, sem_scores, out_of_sample_scores, sem_oos_scores


def iterate_balanced_decoder(training_data, training_labels, testing_data, testing_labels, repeat_number=100,
                             show_iteration_plots=True):
    """
    Repeatedly runs the selected classifiers across the trials time using a balanced selection of trial types.
    The aim is to train and test the classifier with the same number of trials of each type to avoid introducing bias,
    whilst repeating the procedure a sufficient number of times that any error introduced by the sampling is evened out.
    """

    trained_data_scores = np.empty((repeat_number, np.shape(training_data)[2]))
    # all_laser_test_scores = np.empty((repeat_number, np.shape(training_data)[2]))
    out_of_sample_data_scores = np.empty((repeat_number, np.shape(training_data)[2]))
    for rep in range(repeat_number):

        # Balance the non-laser trials
        balance_index = balance_labels(training_labels)

        balanced_trials = training_labels[balance_index]
        balanced_sample = training_data[balance_index, :, :]

        average_scores, sem_scores, test_only_scores, sem_test_only = repeat_across_time(balanced_sample,
                                                                                         balanced_trials,
                                                                                         test_only_data=testing_data,
                                                                                         test_only_labels=testing_labels)

        if show_iteration_plots:
            # Plot the results
            fig = plt.figure()
            plt.errorbar(x_data_time, average_scores, yerr=sem_scores)
            plt.errorbar(x_data_time, test_only_scores, yerr=sem_test_only)
            # plt.plot(x_data_time, laser_scores)
            # plt.legend(['Laser', 'Laser ind', 'Non-laser'])
            plt.legend(['Trained data', 'Out of sample'])
            plt.ylabel('Decoder performance')
            plt.xlabel('Time (s)')
            plt.title('Iteration: ' + str(rep))
            plt.show()

        trained_data_scores[rep, :] = average_scores
        # all_laser_test_scores[rep, :] = laser_scores
        out_of_sample_data_scores[rep, :] = test_only_scores

    return trained_data_scores, out_of_sample_data_scores


def balance_labels(labels):
    """
    Function to fetch the same number of trials of each condition from the list provided.
    """

    unique_labels, counts = np.unique(labels, return_counts=True)
    largest_group_size = np.max(counts)
    smallest_group_size = np.min(counts)
    smallest_group = np.where(counts == smallest_group_size)[0][0]
    largest_group = np.where(counts == largest_group_size)[0][0]

    trials_to_exclude = largest_group_size - smallest_group_size
    balance_index = np.tile(True, np.shape(labels))
    large_group_index = np.where(labels == unique_labels[largest_group])[0]
    if trials_to_exclude == 0:
        return balance_index
    else:
        balance_index[np.random.permutation(large_group_index)[0:trials_to_exclude]] = False

    return balance_index


def sample_trials(model_data, filter_labels, chosen_trials, output_labels=None):
    for ii in range(np.shape(chosen_trials)[0]):
        if ii == 0:
            trial_index = filter_labels == chosen_trials[ii]
        else:
            trial_index = np.logical_or(trial_index, filter_labels == chosen_trials[ii])

    if output_labels is None:
        filtered_output_labels = filter_labels[trial_index]
    else:
        filtered_output_labels = output_labels[trial_index]

    filtered_model_data = model_data[trial_index, :, :]
    unused_labels, trial_counts = np.unique(filtered_output_labels, return_counts=True)

    return filtered_model_data, filtered_output_labels, trial_counts


def basic_plot(time_data, non_laser, laser, title_tag, plot_save_path):
    # Plot the results
    fig = plt.figure()

    # plt.errorbar(x_data_time, np.average(vis1_non_laser_scores, 0), yerr=stats.sem(vis1_non_laser_scores, 0))
    # #plt.errorbar(x_data_time, np.average(vis1_laser_ind, 0), yerr=stats.sem(vis1_laser_ind, 0))
    # plt.errorbar(x_data_time, np.average(vis1_laser_scores, 0), yerr=stats.sem(vis1_laser_scores, 0))

    plt.errorbar(time_data, np.average(non_laser, 0), yerr=np.std(non_laser, 0))
    plt.errorbar(time_data, np.average(laser, 0), yerr=np.std(laser, 0))
    plt.legend(['Non-laser', 'Laser'])
    plt.ylabel('Decoder performance')
    plt.xlabel('Time (s)')
    plt.title(title_tag + ' - Average performance')
    save_p = plot_save_path + r'\average_decoding_performance_' + title_tag
    fig.savefig(save_p)


if __name__ == '__main__':

    root = tk.Tk()
    root.withdraw()

    folder_path = filedialog.askdirectory()
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)
        break

    # Check which level we're at:
    contents_test = []
    for ii in range(len(f)):
        contents_test.append('imgtrl_base_data' in f[ii])

    d = []
    for (subdirpath, subdirnames, subfilenames) in os.walk(os.path.join(dirpath, dirnames[0])):
        d.extend(subfilenames)
        break

    dir1_contents_test = []
    for ii in range(len(d)):
        dir1_contents_test.append('imgtrl_base_data' in d[ii])

    loaded_data = []
    if any(contents_test):
        loaded_data.append(MatLabData(folder_path))
    elif any(dir1_contents_test):
        for jj in range(len(dirnames)):
            # a = SimplifiedMatLabData(os.path.join(dirpath, dirnames[jj]), model_choice='NMF')
            loaded_data.append(MatLabData(os.path.join(dirpath, dirnames[jj])))

    # Perform dimensionality reduction

    # Filter for the desired trial types
    # - Classify laser power in attend condition
    # - Test in alternative condition
    # - Try both ways

    # Plot results of the decoding



    """
    vis1_no_laser_group = []
    vis1_laser_group = []
    vis2_no_laser_group = []
    vis2_laser_group = []
    for kk in range(len(loaded_data)):
        save_path = loaded_data[kk].matlab_data_path + r'\decoding_lasers'
        if not os.path.isdir(save_path):
            os.mkdir(save_path)

        NMF_path = save_path + r'\NMF_model.npy'

        # Filter it for only the visual trials
        filtered_data, visual_trials, time_idx = get_vis_trials(loaded_data[kk].trial_data, loaded_data[kk].bhv,
                                                                loaded_data[kk].trial_time)
        x_data_time = loaded_data[kk].trial_time[time_idx[0]:time_idx[1]]

        num_of_trials = np.shape(filtered_data)[0]
        num_of_neurons = np.shape(filtered_data)[1]
        num_of_time_points = np.shape(filtered_data)[2]

        # Reshapes the array so that it is 2D rather than 3D
        columnar_data = reshape_data(filtered_data)

        # Mean normalise and scale by variance
        scaled_data = normalise_data(columnar_data)

        path_good = os.path.isfile(NMF_path)
        if path_good:
            temp_load = np.load(NMF_path, allow_pickle=True)
            NMF_data = temp_load[0]
            dr_model = temp_load[1]
        else:
            # Run dimensionality reduction on the dataset
            print('Running NMF model')
            chosen_num_of_dims = 20
            NMF_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points,
                                                       chosen_num_of_dims,
                                                       'NMF')
            np.save(NMF_path, [NMF_data, dr_model])

        # Separate off the non-laser trials - split rel/irrel vis1/vis2 - can also try multi-output.
        all_condition_labels = loaded_data[kk].bhv['sel']['condAll'][visual_trials]
        coarse_condition_labels = loaded_data[kk].bhv['sel']['condCoarse'][visual_trials]

        # Vis 1 decoding
        # Get just the vis1 no-laser rel and irrel trials
        typed_sample, no_laser_vis1_trials, non_laser_counts = sample_trials(NMF_data, all_condition_labels, [1, 7],
                                                                             coarse_condition_labels)
        print(str(non_laser_counts[0]) + ' rel vis1 non-laser trials, ' + str(
            non_laser_counts[1]) + ' irrel vis1 non-laser trials.')

        # Get just the vis1 laser rel and irrel trials
        laser_sample, laser_vis1_trials, laser_counts = sample_trials(NMF_data, all_condition_labels, [3, 9],
                                                                      coarse_condition_labels)
        print(str(laser_counts[0]) + ' rel vis1 laser trials, ' + str(laser_counts[1]) + ' irrel vis1 laser trials.')

        print('Decoding Vis 1 rel vs irrel')
        vis1_non_laser_scores, vis1_laser_scores, vis1_laser_ind = iterate_balanced_decoder(typed_sample,
                                                                                      no_laser_vis1_trials,
                                                                                      laser_sample, laser_vis1_trials)

        print('Plotting and saving the results')
        basic_plot(x_data_time, vis1_non_laser_scores, vis1_laser_scores, 'Vis 1', save_path)

        # Vis 2 decoding
        # Get just the vis2 no-laser rel and irrel trials
        typed_sample, no_laser_vis2_trials, non_laser_counts = sample_trials(NMF_data, all_condition_labels, [4, 10],
                                                                             coarse_condition_labels)
        print(str(non_laser_counts[0]) + ' rel vis2 non-laser trials, ' + str(
            non_laser_counts[1]) + ' irrel vis2 non-laser trials.')

        # Get just the vis2 laser rel and irrel trials
        laser_sample, laser_vis2_trials, laser_counts = sample_trials(NMF_data, all_condition_labels, [6, 12],
                                                                      coarse_condition_labels)
        print(str(laser_counts[0]) + ' rel vis2 laser trials, ' + str(laser_counts[1]) + ' irrel vis2 laser trials.')

        print('Decoding Vis 2 rel vs irrel')
        vis2_non_laser_scores, vis2_laser_scores, vis2_laser_ind = iterate_balanced_decoder(typed_sample,
                                                                                      no_laser_vis2_trials,
                                                                                      laser_sample, laser_vis2_trials)
        print('Plotting and saving the results')
        basic_plot(x_data_time, vis2_non_laser_scores, vis2_laser_scores, 'Vis 2', save_path)

        vis1_no_laser_group.append(np.average(vis1_non_laser_scores, axis=0))
        vis1_laser_group.append(np.average(vis1_laser_scores, axis=0))
        vis2_no_laser_group.append(np.average(vis2_non_laser_scores, axis=0))
        vis2_laser_group.append(np.average(vis2_laser_scores, axis=0))

    print(vis1_no_laser_group)
    min_group = []
    for sess in range(len(vis1_no_laser_group)):
        min_group.append(vis1_no_laser_group[sess].size)

    group_length = min(min_group)

    vis1_no_laser_average = []
    vis1_laser_average = []
    vis2_no_laser_average = []
    vis2_laser_average = []
    for sess in range(len(vis1_no_laser_group)):
        vis1_no_laser_average = np.append(vis1_no_laser_average, vis1_no_laser_group[sess][
            np.round(np.linspace(0, vis1_no_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)
        vis1_laser_average = np.append(vis1_laser_average, vis1_laser_group[sess][
            np.round(np.linspace(0, vis1_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)
        vis2_no_laser_average = np.append(vis2_no_laser_average, vis2_no_laser_group[sess][
            np.round(np.linspace(0, vis2_no_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)
        vis2_laser_average = np.append(vis2_laser_average, vis2_laser_group[sess][
            np.round(np.linspace(0, vis2_laser_group[sess].size - 1, group_length)).astype(int)], axis=0)

    """

    # for mm in range(len(vis1_no_laser_group)):

    # # Try to decode laser from non-laser
    #
    # rel_trials = np.logical_or(all_condition_labels == 1, all_condition_labels == 3)
    # rel_labels = all_condition_labels[rel_trials]
    # typed_sample = NMF_data[rel_trials, :, :]
    #
    # # Balance the non-laser trials
    # print(str(sum(rel_labels == 1)) + ' no laser vis1 trials, ' + str(
    #     sum(rel_labels == 3)) + ' laser vis1 trials.')
    # balance_index = balance_labels(rel_labels)
    #
    # balanced_trials = rel_labels[balance_index]
    # balanced_sample = typed_sample[balance_index, :, :]
    #
    # # Train the decoder on the non-laser trials - one time point at a time
    # average_scores = []
    # sem_scores = []
    # for time_column in range(np.shape(balanced_sample)[2]):
    #     time_sample = balanced_sample[:, :, time_column]
    #
    #     # Test the decoder on laser and non-laser trials
    #     average_clf, scores = run_decoder(time_sample, balanced_trials, method="logistic regression")
    #     average_scores.append(np.average(scores))
    #     sem_scores.append(stats.sem(scores))
    #
    # # Plot the results
    # fig = plt.figure()
    # plt.errorbar(x_data_time, average_scores, yerr=sem_scores)
    # plt.legend(['Rel trials'])
    # plt.ylabel('Decoder performance')
    # plt.xlabel('Time (s)')
    # plt.show()

    #
    # # Loop through dimension numbers and get laser decoder performance
    # print('Calculating laser decoding performance for different dimensionalities...')
    # rel_vis1_trials = np.logical_or(all_condition_labels == 1, all_condition_labels == 3)
    # rel_vis1_labels = all_condition_labels[rel_vis1_trials]
    #
    # rel_vis2_trials = np.logical_or(all_condition_labels == 4, all_condition_labels == 6)
    # rel_vis2_labels = all_condition_labels[rel_vis2_trials]
    #
    # irrel_vis1_trials = np.logical_or(all_condition_labels == 7, all_condition_labels == 9)
    # irrel_vis1_labels = all_condition_labels[irrel_vis1_trials]
    #
    # irrel_vis2_trials = np.logical_or(all_condition_labels == 10, all_condition_labels == 12)
    # irrel_vis2_labels = all_condition_labels[irrel_vis2_trials]
    #
    # r_vis1_scores = []
    # r_vis2_scores = []
    # i_vis1_scores = []
    # i_vis2_scores = []
    # r_vis1_sem = []
    # r_vis2_sem = []
    # i_vis1_sem = []
    # i_vis2_sem = []
    # NMF_models = []
    # NMF_model_data = []
    # NMF_path = save_path + r'\NMF_dimension_test.npy'
    # for dim_number in np.linspace(1, 50, 50):
    #     print('NNMF with dimensionality :' + str(int(dim_number)))
    #     NMF_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points, int(dim_number),
    #                                                'NMF')
    #     NMF_models.append(dr_model)
    #     NMF_model_data.append(NMF_data)
    #     # Calculate for vis1 attending
    #     typed_sample = NMF_data[rel_vis1_trials, :, :]
    #     balance_index = balance_labels(rel_vis1_labels)
    #     balanced_sample = typed_sample[balance_index, :, :]
    #     balanced_trials = rel_vis1_labels[balance_index]
    #
    #     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
    #     r_vis1_scores.append(np.average(average_scores))
    #     r_vis1_sem.append(stats.sem(average_scores))
    #
    #     # Calculate for vis2 attending
    #     typed_sample = NMF_data[rel_vis2_trials, :, :]
    #     balance_index = balance_labels(rel_vis2_labels)
    #     balanced_sample = typed_sample[balance_index, :, :]
    #     balanced_trials = rel_vis2_labels[balance_index]
    #
    #     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
    #     r_vis2_scores.append(np.average(average_scores))
    #     r_vis2_sem.append(stats.sem(average_scores))
    #
    #     # Calculate for vis2 ignore
    #     typed_sample = NMF_data[irrel_vis1_trials, :, :]
    #     balance_index = balance_labels(irrel_vis1_labels)
    #     balanced_sample = typed_sample[balance_index, :, :]
    #     balanced_trials = irrel_vis1_labels[balance_index]
    #
    #     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
    #     i_vis1_scores.append(np.average(average_scores))
    #     i_vis1_sem.append(stats.sem(average_scores))
    #
    #     # Calculate for vis1 attending
    #     typed_sample = NMF_data[irrel_vis2_trials, :, :]
    #     balance_index = balance_labels(irrel_vis2_labels)
    #     balanced_sample = typed_sample[balance_index, :, :]
    #     balanced_trials = irrel_vis2_labels[balance_index]
    #
    #     average_scores, sem = repeat_across_time(balanced_sample, balanced_trials)
    #     i_vis2_scores.append(np.average(average_scores))
    #     i_vis2_sem.append(stats.sem(average_scores))
    #
    # np.save(NMF_path, [NMF_model_data, NMF_models])
    # lowerY = np.empty((2, 1))
    # upperY = np.empty((2, 1))
    # fig = plt.figure()
    # ax1 = plt.subplot(2, 1, 1)
    # rv1 = plt.errorbar(np.linspace(1, 50, 50), r_vis1_scores, yerr=r_vis1_sem)
    # iv1 = plt.errorbar(np.linspace(1, 50, 50), i_vis1_scores, yerr=i_vis1_sem)
    # lowerY[0], upperY[0] = plt.ylim()
    # plt.hlines(0.5, 0, 50, linestyles='dotted')
    # plt.xticks(np.arange(1, 51, step=1))
    # plt.legend((rv1, iv1), ('Rel vis1', 'Irrel vis1'))
    # plt.ylabel('Average decoder performance')
    # plt.xlabel('Dimensions of NMF')
    # plt.title('Decoding laser from non-laser')
    #
    # ax2 = plt.subplot(2, 1, 2)
    # rv2 = plt.errorbar(np.linspace(1, 50, 50), r_vis2_scores, yerr=r_vis2_sem)
    # iv2 = plt.errorbar(np.linspace(1, 50, 50), i_vis2_scores, yerr=i_vis2_sem)
    # lowerY[1], upperY[1] = plt.ylim()
    # plt.hlines(0.5, 0, 50, linestyles='dotted')
    # plt.xticks(np.arange(1, 51, step=1))
    # plt.legend((rv2, iv2), ('Rel vis2', 'Irrel vis2'))
    # plt.ylabel('Average decoder performance')
    # plt.xlabel('Dimensions of NMF')
    # plt.title('Decoding laser from non-laser')
    #
    # axisLimits = [lowerY.min(), upperY.max()]
    # ax1.set_ylim(axisLimits)
    #
    # save_p = save_path + r'\laser_decoding_across_dims'
    # fig.savefig(save_p)
    # plt.show()
