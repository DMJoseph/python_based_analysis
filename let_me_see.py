import itertools
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import os


def sub_cond_plot(data_frame, sub_cond_path, dims_to_plot):
    sns.set(style="darkgrid")

    for cond_idx in np.unique(data_frame.coarse_conds):
        h1 = sns.pairplot(data_frame.query(f'coarse_conds == {cond_idx}'), vars=['dim0', 'dim1', 'dim2', 'dim3'],
                          hue='all_conds', plot_kws=dict(s=20, alpha=0.5))

        h2 = sns.pairplot(data_frame.query(f'coarse_conds == {cond_idx}'), vars=['dim0', 'dim1', 'dim2', 'dim3'],
                          hue='outcome', plot_kws=dict(s=20, alpha=0.5))

        path_good = os.path.isdir(sub_cond_path)
        if not path_good:
            os.mkdir(sub_cond_path)

        h1.savefig(sub_cond_path + r'\sub_cond' + str(cond_idx))
        h2.savefig(sub_cond_path + r'\outcome' + str(cond_idx))


def overview_plot(data_frame, save_p, dims_to_plot):
    sns.set(style="darkgrid")
    dim_numbers = range(dims_to_plot)
    dim_variable = ['dim'] * dims_to_plot
    for idx in range(dims_to_plot):
        #dim_variable[idx].join([str(i)for i in range(8)])
        dim_variable[idx] = dim_variable[idx] + str(idx)

    all_permutations = list(itertools.permutations(dim_variable, 4))

    h = sns.pairplot(data_frame, vars=dim_variable[0:4], hue='coarse_conds', plot_kws=dict(s=20, alpha=0.5))
    h.savefig(save_p)


def plot_PCA_scores(pca_model, save_path):
    f1 = plt.figure()
    plt.plot(pca_model.explained_variance_)
    plt.xlabel('Dimension')
    plt.ylabel('Variance explained')
    plt.title('Explained Variance')

    f2 = plt.figure()
    plt.plot(pca_model.explained_variance_ratio_)
    plt.xlabel('Dimension')
    plt.ylabel('Percent')
    plt.title('Percent explained Variance')

    f1.savefig(save_path + r'\explained_variance')
    f2.savefig(save_path + r'\percent_explained_variance')