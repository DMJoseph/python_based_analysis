import scipy.io as spio
import numpy as np
import sklearn.decomposition as skl
import matplotlib.pyplot as plt

trial_data = spio.loadmat(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_testData.mat', squeeze_me=True)
cond_coarse = spio.loadmat(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\cond_coarse.mat', squeeze_me=True)

cond_coarse = cond_coarse['cond_coarse']
odour_trials = np.logical_or(cond_coarse == 5, cond_coarse == 6)
visual_trials = np.logical_not(odour_trials)
a = trial_data['VHS1_testData']
a = a[visual_trials, :, 26:]
num_of_trials = np.shape(a)[0]
num_of_neurons = np.shape(a)[1]
num_of_timepoints = np.shape(a)[2]


print(f'Array is {num_of_trials} trials, {num_of_neurons} neurons, {num_of_timepoints} timepoints.')

reshapedData = np.empty((num_of_trials*num_of_timepoints, num_of_neurons))
for neuron in range(num_of_neurons):
    reshapedData[:, neuron] = np.ndarray.reshape(a[:, neuron, :], (num_of_timepoints * num_of_trials))

print('Fitting model')
positiveData = reshapedData + abs(np.amin(reshapedData))
num_of_components = 20
model = skl.NMF(n_components=num_of_components, max_iter=10000).fit_transform(positiveData)

print('Reshaping fitted and transformed data')
reshapedTransform = np.empty((num_of_trials, num_of_components, num_of_timepoints))
for dim in range(num_of_components):
    reshapedTransform[:, dim, :] = np.ndarray.reshape(model[:, dim], (num_of_trials, num_of_timepoints))

print('Plotting data')
vis_conds = cond_coarse[visual_trials]
fig = plt.figure()
for visOne in np.argwhere(vis_conds == 1):
    print(visOne[0])
    plt.plot(reshapedTransform[visOne[0], 0, :], reshapedTransform[visOne[0], 1, :])
    print(f'displaying trial: {visOne[0]}')
    plt.pause(5)