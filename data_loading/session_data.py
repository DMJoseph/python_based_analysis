import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import FactorAnalysis, PCA, NMF
from datetime import datetime
import matplotlib.pyplot as plt
import os
import mat73
import palettable.colorbrewer.qualitative as colourPalette
from scipy.io import loadmat

class MatLabData:
    """A simple class to load the data for a single session and store it under one header"""

    def __init__(self, session_dir):
        self.matlab_data_path = session_dir

        # Loads the trial by trial neural data, the behavioural data
        # and the time value of the trial time index of the neural data.
        print('Loading base data and neural filters...', end="")

# Todo: make this try/catch or something.
        saved_mat_data = mat73.loadmat(os.path.join(self.matlab_data_path, 'imgtrl_base_data.mat'))
        # saved_mat_data = loadmat(os.path.join(self.matlab_data_path, 'imgtrl_base_data.mat'))
        self.trial_data = saved_mat_data['trial_data']
        self.bhv = saved_mat_data['behave_data']
        self.trial_time = saved_mat_data['trial_time']
        self.session_name = saved_mat_data['session_name']
        self.number_of_trials = np.shape(self.trial_data)[0]
        self.number_of_neurons = np.shape(self.trial_data)[1]
        self.number_of_time_points = np.shape(self.trial_data)[2]
        self.selectivity_data = saved_mat_data['selectivity_data']
        self.cell_label =  saved_mat_data['cell_label']

        print('done')
        print(
            f'Session has {self.number_of_trials} trials, {self.number_of_neurons} neurons, {self.number_of_time_points} time-points.')


class SimplifiedMatLabData(MatLabData):
    """A subclass of MatLab data including with data reshaped and rearranged ready for dimensionality reduction"""

    def __init__(self, session_dir, model_choice='PCA', number_of_components=20, visual_filter='all',
                 attention_filter='all', laser_filter='all',
                 neuron_group='all'):

        super().__init__(session_dir)
        # Set up the filters that will be used by filter_data_axes()
        self.visual_stimulus = visual_filter
        self.attention = attention_filter
        self.chosen_lasers = laser_filter
        self.neuron_filter = neuron_group

        self.filtered_data = []
        self.filtered_trial_index = []
        self.filtered_neuron_index = []
        self.time_index = []
        self.num_filtered_trials = []
        self.num_filtered_neurons = []
        self.num_filtered_time_points = []
        # Filter the data for just visual trials between certain time-points.
        self.filter_data_axes()

        # Reshape the data so that it's in columns
        self.reshaped_data = []
        self.reshape_data()

        # Normalise data so that it has mean 0 and standard deviation 1
        self.normalised_data = []
        self.normalise_data()

        self.number_of_components = number_of_components
        self.model_type = model_choice
        self.model_data = []
        self.model = []

        self.model_save_dir = self.matlab_data_path + r'\\model'
        if not os.path.isdir(self.model_save_dir):
            os.mkdir(self.model_save_dir)
        self.model_check_and_load()

    def filter_data_axes(self, time_limits=None):

        if time_limits is None:
            time_limits = (-0.5, 1)

        print('Filtering dF for trials...', end="")
        cond_coarse = self.bhv['sel']['condCoarse']
        laser_power = self.bhv['sel']['laserPower']
        max_laser = laser_power.max

        # Set up the trial filtering
        # Filter by visual stimulus type
        if self.visual_stimulus == 'all':
            visual_filter = np.logical_and(cond_coarse != 5, cond_coarse != 6)
        elif self.visual_stimulus == 'rewarded':
            visual_filter = np.logical_or(cond_coarse == 1, cond_coarse == 3)
        elif self.visual_stimulus == 'rewarded':
            visual_filter = np.logical_or(cond_coarse == 2, cond_coarse == 4)

        # Filter by attention
        if self.attention == 'all':
            attention_filter = np.logical_and(cond_coarse != 5, cond_coarse != 6)
        elif self.attention == 'attend':
            attention_filter = np.logical_or(cond_coarse == 1, cond_coarse == 2)
        elif self.attention == 'ignore':
            attention_filter = np.logical_or(cond_coarse == 3, cond_coarse == 4)

        # Filter by laser power
        if self.chosen_lasers == 'all':
            laser_filter = np.ones_like(laser_power)
        elif self.chosen_lasers == 'zero':
            laser_filter = laser_power == 0
        elif self.chosen_lasers == 'max':
            laser_filter = laser_power == max_laser

        temp_filter = np.logical_and(visual_filter, attention_filter)
        trial_filter = np.logical_and(temp_filter, laser_filter)

        # Set up the neuron filtering
        if self.neuron_filter == 'all':
            neuron_index = np.ones(self.number_of_neurons)
        elif self.neuron_filter == 'sig_selective':
            neuron_index = self.selectivity_data['sig_selective'] < 0.01
        elif self.neuron_filter == 'sig_increasing':
            neuron_index = np.logical_and(self.selectivity_data['sig_change'],
                                          -1 == self.selectivity_data['sig_change_dir'])

        time_idx = [np.nonzero(self.trial_time >= time_limits[0])[0][0],
                    np.nonzero(self.trial_time >= time_limits[1])[0][0]]

        time_filter = np.zeros_like(self.trial_time)
        time_filter[time_idx[0]:time_idx[1]] = 1
        time_filter = time_filter == 1

        # Don't really understand broadcasting yet so I'm using this workaround
        trial_filtered_data = self.trial_data[trial_filter, :, :]
        second_filter = trial_filtered_data[:, neuron_index, :]
        test_data = second_filter[:, :, time_filter]

        self.filtered_data = test_data
        self.filtered_trial_index = trial_filter
        self.time_index = time_idx
        self.num_filtered_trials = np.shape(test_data)[0]
        self.num_filtered_neurons = np.shape(test_data)[1]
        self.num_filtered_time_points = np.shape(test_data)[2]
        print('done')
        print(
            f'Filtered data has {self.num_filtered_trials} trials, {self.num_filtered_neurons} neurons, {self.num_filtered_time_points} time-points.')

        return test_data

    def reshape_data(self):
        print('Reshaping filtered data into columnar format...', end="")

        for neuron in range(self.num_filtered_neurons):
            neuron_data = self.filtered_data[:, neuron, :].reshape(
                (self.num_filtered_time_points * self.num_filtered_trials, 1))
            if neuron == 0:
                reshaped_data = neuron_data
            else:
                reshaped_data = np.append(reshaped_data, neuron_data, axis=1)

        self.reshaped_data = reshaped_data

        print('done')
        return reshaped_data

    def normalise_data(self):
        print('Normalising data...', end="")
        scaled_data = np.empty(np.shape(self.reshaped_data))
        for neuron in range(self.num_filtered_neurons):
            scaler = StandardScaler()
            fit_neuron = scaler.fit_transform(self.reshaped_data[:, neuron].reshape(-1, 1))
            scaled_data[:, neuron] = fit_neuron.reshape(np.size(fit_neuron))

        print('done')
        self.normalised_data = scaled_data

        return scaled_data

    def run_model(self):
        if self.model_type == 'FA':
            # print('Running FA.')
            chosen_model = FactorAnalysis(n_components=self.number_of_components).fit(self.normalised_data)
        elif self.model_type == 'PCA':
            # print('Running PCA.')
            chosen_model = PCA(n_components=self.number_of_components).fit(self.normalised_data)
        elif self.model_type == 'NMF':
            # print('Running NMF.')
            smallest_value = self.normalised_data.min()
            self.normalised_data = self.normalised_data + abs(smallest_value)
            chosen_model = NMF(n_components=self.number_of_components, max_iter=10000).fit(self.normalised_data)

        transformed_data = chosen_model.transform(self.normalised_data)

        reshaped_transform = np.empty(
            (self.num_filtered_trials, self.number_of_components, self.num_filtered_time_points))
        for dim in range(self.number_of_components):
            reshaped_transform[:, dim, :] = np.ndarray.reshape(transformed_data[:, dim], (
                self.num_filtered_trials, self.num_filtered_time_points))

        self.model_data = reshaped_transform
        self.model = chosen_model

        return reshaped_transform, chosen_model

    def model_check_and_load(self):
        self.model_load()

        parameter_check = [self.num_filtered_trials == np.shape(self.model_data)[0],
                           self.number_of_components == np.shape(self.model_data)[1],
                           self.num_filtered_time_points == np.shape(self.model_data)[2]]
        if not all(parameter_check):
            print('Filtered data and loaded model do not match - rebuilding...')
            self.model_load(force_build=True)

    def model_load(self, force_build=False):
        model_save_path = self.model_save_dir + r'\\' + self.model_type
        path_good = os.path.isfile(model_save_path)
        if path_good and not force_build:
            print('Loading saved version of the model...', end="")
            temp_load = np.load(model_save_path, allow_pickle=True)
            self.model_data = temp_load[0]
            self.model = temp_load[1]
            print('done')
        else:
            # Run dimensionality reduction on the data-set
            print('Running ' + self.model_type + ' model on data-set', end="")
            self.run_model()
            np.save(model_save_path, [self.model_data, self.model])

            model_backup_path = model_save_path + datetime.now().strftime("%Y%m%d_%H%M%S")
            np.save(model_backup_path, [self.model_data, self.model])
            print('done')

    def visualise_two_dimensions(self, dimensions_chosen=(0, 1)):

        filtered_time = self.trial_time[self.time_index[0]:self.time_index[1]]
        zero_point = np.nonzero(filtered_time >= 0)[0][0]

        trial_type_label = self.bhv['sel']['condAll'][self.filtered_trial_index]
        unique_labels = np.unique(trial_type_label)
        colour_map = colourPalette.Pastel1_8
        average_trajectories = np.empty((len(unique_labels), 2, np.shape(self.model_data)[2]))
        average_trajectories[:] = np.nan

        for condition in unique_labels:
            f1 = plt.figure()
            cond_index = np.where(unique_labels == condition)
            sub_trial_index = np.where(trial_type_label == condition)

            for trial in sub_trial_index[0]:
                plt.plot(np.squeeze(self.model_data[trial, dimensions_chosen[0], :]),
                         np.squeeze(self.model_data[trial, dimensions_chosen[1], :]),
                         color=colour_map.mpl_colors[cond_index[0][0]])

                # Add key time points
                # start
                plt.plot(np.squeeze(self.model_data[trial, dimensions_chosen[0], 0]),
                         np.squeeze(self.model_data[trial, dimensions_chosen[1], 0]),
                         'ok')
                # stimulus onset
                plt.plot(np.squeeze(self.model_data[trial, dimensions_chosen[0], zero_point]),
                         np.squeeze(self.model_data[trial, dimensions_chosen[1], zero_point]),
                         'ob')

                # End
                plt.plot(np.squeeze(self.model_data[trial, dimensions_chosen[0], -1]),
                         np.squeeze(self.model_data[trial, dimensions_chosen[1], -1]),
                         'or')

            plt.xlabel(['Dimension_' + str(dimensions_chosen[0])])
            plt.ylabel(['Dimension_' + str(dimensions_chosen[0])])
            plt.title('Visualised trajectories')

            trial_average = np.mean(self.model_data[sub_trial_index, :, :], axis=1)
            average_trajectories[cond_index[0][0], :, :] = trial_average[:, dimensions_chosen, :]

        avg_fig = plt.figure()
        for group in range(len(unique_labels)):
            plt.plot(np.squeeze(average_trajectories[group, 0, :]),
                     np.squeeze(average_trajectories[group, 1, :]),
                     color=colour_map.mpl_colors[group],
                     label=self.bhv['sel']['condAllLabel'][int(unique_labels[group] - 1)])
            # Add key time points
            # start
            plt.plot(np.squeeze(average_trajectories[group, 0, 0]),
                     np.squeeze(average_trajectories[group, 1, 0]),
                     'ok')
            # stimulus onset
            plt.plot(np.squeeze(average_trajectories[group, 0, zero_point]),
                     np.squeeze(average_trajectories[group, 1, zero_point]),
                     'ob')

            # End
            plt.plot(np.squeeze(average_trajectories[group, 0, -1]),
                     np.squeeze(average_trajectories[group, 1, -1]),
                     'or')

        plt.legend()
        plt.show()

    def visualise_three_dimensions(self, dimensions_chosen=(0, 1, 2)):

        filtered_time = self.trial_time[self.time_index[0]:self.time_index[1]]
        zero_point = np.nonzero(filtered_time >= 0)[0][0]

        trial_type_label = self.bhv['sel']['condAll'][self.filtered_trial_index]
        unique_labels = np.unique(trial_type_label)
        colour_map = colourPalette.Pastel1_8
        average_trajectories = np.empty((len(unique_labels), 3, np.shape(self.model_data)[2]))
        average_trajectories[:] = np.nan

        for condition in unique_labels:
            f1 = plt.figure()
            ax = f1.add_subplot(projection='3d')
            cond_index = np.where(unique_labels == condition)
            sub_trial_index = np.where(trial_type_label == condition)

            for trial in sub_trial_index[0]:
                ax.plot3D(np.squeeze(self.model_data[trial, dimensions_chosen[0], :]),
                          np.squeeze(self.model_data[trial, dimensions_chosen[1], :]),
                          np.squeeze(self.model_data[trial, dimensions_chosen[2], :]),
                          color=colour_map.mpl_colors[cond_index[0][0]])

                # Add key time points
                # start
                ax.scatter3D(np.squeeze(self.model_data[trial, dimensions_chosen[0], 0]),
                             np.squeeze(self.model_data[trial, dimensions_chosen[1], 0]),
                             np.squeeze(self.model_data[trial, dimensions_chosen[2], 0]),
                             c='k')
                # stimulus onset
                ax.scatter3D(np.squeeze(self.model_data[trial, dimensions_chosen[0], zero_point]),
                             np.squeeze(self.model_data[trial, dimensions_chosen[1], zero_point]),
                             np.squeeze(self.model_data[trial, dimensions_chosen[2], zero_point]),
                             c='b')

                # End
                ax.scatter3D(np.squeeze(self.model_data[trial, dimensions_chosen[0], -1]),
                             np.squeeze(self.model_data[trial, dimensions_chosen[1], -1]),
                             np.squeeze(self.model_data[trial, dimensions_chosen[2], -1]),
                             c='r')

            plt.xlabel(['Dimension_' + str(dimensions_chosen[0])])
            plt.ylabel(['Dimension_' + str(dimensions_chosen[0])])
            plt.title('Visualised trajectories')

            trial_average = np.mean(self.model_data[sub_trial_index, :, :], axis=1)
            average_trajectories[cond_index[0][0], :, :] = trial_average[:, dimensions_chosen, :]

        avg_fig = plt.figure()
        ax = avg_fig.add_subplot(projection='3d')
        for group in range(len(unique_labels)):
            ax.plot3D(np.squeeze(average_trajectories[group, 0, :]),
                      np.squeeze(average_trajectories[group, 1, :]),
                      np.squeeze(average_trajectories[group, 2, :]),
                      color=colour_map.mpl_colors[group],
                      label=self.bhv['sel']['condAllLabel'][int(unique_labels[group] - 1)])
            # Add key time points
            # start
            ax.scatter3D(np.squeeze(average_trajectories[group, 0, 0]),
                         np.squeeze(average_trajectories[group, 1, 0]),
                         np.squeeze(average_trajectories[group, 2, 0]),
                         c='k')
            # stimulus onset
            ax.scatter3D(np.squeeze(average_trajectories[group, 0, zero_point]),
                         np.squeeze(average_trajectories[group, 1, zero_point]),
                         np.squeeze(average_trajectories[group, 2, zero_point]),
                         c='b')

            # End
            ax.scatter3D(np.squeeze(average_trajectories[group, 0, -1]),
                         np.squeeze(average_trajectories[group, 1, -1]),
                         np.squeeze(average_trajectories[group, 2, -1]),
                         c='r')

        plt.legend()
        plt.show()
