import mat73
import numpy as np
import pandas as pd

class ParadoxData:
    """ Simple class to load the data from PV paradoxical responses sessions for decoding"""

    def __init__(self, session_file):
        self.matlab_data_path = session_file

        saved_mat_data = mat73.loadmat(self.matlab_data_path)

        filter_for_pv = True
        if filter_for_pv:
            neuron_filter = np.logical_not(saved_mat_data['storedData']['labelledNeuron'])
        else:
            neuron_filter = np.ones_like(saved_mat_data['storedData']['labelledNeuron'])

        self.session = saved_mat_data['storedData']['sessName']
        self.selectivity = saved_mat_data['storedData']['selectivity'][neuron_filter]
        self.delta_sel = saved_mat_data['storedData']['deltaSel'][neuron_filter]
        self.green = saved_mat_data['storedData']['greenFluor'][neuron_filter]
        self.red = saved_mat_data['storedData']['redFluor'][neuron_filter]
        self.pool_sd = saved_mat_data['storedData']['poolStd'][neuron_filter]
        self.size = saved_mat_data['storedData']['roiSize'][neuron_filter]
        self.vis1_mean = saved_mat_data['storedData']['vis1mean'][neuron_filter, :]
        self.vis1_laser_diff = self.vis1_mean[:, 2] - self.vis1_mean[:, 1]
        self.vis2_mean = saved_mat_data['storedData']['vis2mean'][neuron_filter, :]
        self.vis2_laser_diff = self.vis2_mean[:, 2] - self.vis2_mean[:, 1]
        self.PV_index = saved_mat_data['storedData']['labelledNeuron'][neuron_filter]

        cat_data = np.concatenate((self.selectivity.reshape((-1, 1)), self.delta_sel.reshape((-1, 1)), self.green.reshape((-1, 1)), self.red.reshape((-1, 1)), self.pool_sd.reshape((-1, 1)), self.size.reshape((-1, 1)), self.PV_index.reshape((-1, 1))), axis=1)
        self.base_df = pd.DataFrame(cat_data, columns=['sel', 'delta_sel', 'green', 'red', 'pool_sd', 'size', 'PV'])
