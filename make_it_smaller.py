import numpy as np
from sklearn.decomposition import FactorAnalysis, PCA, NMF
from sklearn.manifold import TSNE
from data_preparation import load_data, get_vis_trials, reshape_data, prepare_data_for_seaborn, normalise_data
import matplotlib.pyplot as plt
import let_me_see
import os


def run_model(neural_data, trial_number, time_number, number_components, model_choice='PCA'):
    if model_choice == 'FA':
        print('Running FA.')
        chosen_model = FactorAnalysis(n_components=number_components).fit(neural_data)
    elif model_choice == 'PCA':
        print('Running PCA.')
        chosen_model = PCA(n_components=number_components).fit(neural_data)
    elif model_choice == 'NMF':
        print('Running NMF.')
        smallest_value = neural_data.min()
        neural_data = neural_data + abs(smallest_value)
        chosen_model = NMF(n_components=number_components, max_iter=10000).fit(neural_data)

    print('Transforming data.')
    transformed_data = chosen_model.fit_transform(neural_data)

    reshaped_transform = np.empty((trial_number, number_components, time_number))
    for dim in range(number_components):
        reshaped_transform[:, dim, :] = np.ndarray.reshape(transformed_data[:, dim], (trial_number, time_number))

    return reshaped_transform, model_choice, chosen_model


def score_components(reshaped_data, save_path, max_components=50, model_choice='PCA'):
    score_values = np.empty(max_components)

    for num_of_components in range(max_components):
        if model_choice == 'FA':
            dr_model = FactorAnalysis(n_components=num_of_components + 1).fit(reshaped_data)
            score_values[num_of_components] = dr_model.score(reshaped_data)
        elif model_choice == 'PCA':
            dr_model = PCA(n_components=num_of_components + 1).fit(reshaped_data)
            score_values[num_of_components] = dr_model.score(reshaped_data)
        elif model_choice == 'NMF':
            smallest_value = reshaped_data.min()
            positive_data = reshaped_data + abs(smallest_value)
            dr_model = NMF(n_components=num_of_components + 1, max_iter=10000).fit(positive_data)
            score_values[num_of_components] = dr_model.reconstruction_err_

    fig = plt.figure()
    plt.plot(range(max_components), score_values)
    plt.xlabel('Dimension')
    plt.ylabel('Score')
    plt.title('Score')
    fig.savefig(save_path + r'\score')

    return score_values


def plot_tsne(test_data, cond_all, visual_trials):

    num_of_trials = np.shape(test_data)[0]
    num_of_neurons = np.shape(test_data)[1]
    num_of_timepoints = np.shape(test_data)[2]

    condition_array = np.tile(cond_all[visual_trials], (num_of_timepoints,1)).transpose()
    labels = np.ndarray.reshape(condition_array, (num_of_timepoints * num_of_trials))
    reshaped_data = np.empty((num_of_trials * num_of_timepoints, num_of_neurons))

    for neuron in range(num_of_neurons):
        reshaped_data[:, neuron] = np.ndarray.reshape(test_data[:, neuron, :], (num_of_timepoints * num_of_trials))

    perp_values = [50, 5, 2]
    for perp in perp_values:
        # perform t-SNE
        tsne_model = TSNE(n_components=2, perplexity=perp, random_state=2020)

        embed = tsne_model.fit_transform(reshaped_data, labels)
        visualize_components(embed[:, 0], embed[:, 1], labels, show=False)
        plt.title(f"perplexity: {perp}")
        plt.show()


def visualize_components(component1, component2, labels, show=True):
    """
                Plots a 2D representation of the data for visualization with categories
                labelled as different colors.

                Args:
                  component1 (numpy array of floats) : Vector of component 1 scores
                  component2 (numpy array of floats) : Vector of component 2 scores
                  labels (numpy array of floats)     : Vector corresponding to categories of
                                                       samples

                Returns:
                  Nothing.

                """

    plt.figure()
    #cmap = plt.cm.get_cmap('tab12')
    plt.scatter(x=component1, y=component2, c=labels)#, cmap=cmap)
    plt.xlabel('Component 1')
    plt.ylabel('Component 2')
    plt.colorbar(ticks=range(12))
    #plt.clim(-0.5, 9.5)
    if show:
        plt.show()


if __name__ == '__main__':
    # Begins the script like execution here.
    plot_tag = '0001' # Replace with an extracted filename
    #data_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_spiking_0001.mat'
    data_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_df_data.mat'
    bhv_data_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_bhv_data.mat'
    time_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\trial_time.mat'
    save_path = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\output'

    # Load and pull the data from the matlab file
    trial_data, bhv, trial_time = load_data(data_p, bhv_data_p, time_p)

    # Filter it for only the visual trials
    filtered_data, visual_trials, time_idx = get_vis_trials(trial_data, bhv, trial_time)
    num_of_trials = np.shape(filtered_data)[0]
    num_of_neurons = np.shape(filtered_data)[1]
    num_of_time_points = np.shape(filtered_data)[2]

    # Reshapes the array so that it is 2D rather than 3D
    columnar_data = reshape_data(filtered_data)

    # Mean normalise and scale by variance
    scaled_data = normalise_data(columnar_data)



    # # Run T-SNE temporary placement
    # #plot_tsne(filtered_data, bhv['sel']['condAll'], visual_trials)
    #
    # # Run the different models
    # print('Scoring PCA at different dimensions')
    # #PCA_scores = score_components(columnar_data, num_of_neurons, model_choice='PCA')
    # # explained variance
    # print('Running PCA for 100 components')
    # PCA_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points, num_of_neurons, 'PCA')
    # print('Reshaping data for seaborn')
    # PCA_data_frame = prepare_data_for_seaborn(PCA_data, bhv, trial_time[time_idx[0]:time_idx[1]], visual_trials, 'low_d')
    #
    # # Plot the results
    # print('Plotting and saving the results')
    # PCA_path = save_path + r'\PCA_model'
    # path_good = os.path.isdir(PCA_path)
    # if not path_good:
    #     os.mkdir(PCA_path)
    #
    # let_me_see.plot_PCA_scores(dr_model, PCA_path)
    #
    # big_save = PCA_path + r'\overview_plot'
    # let_me_see.overview_plot(PCA_data_frame, big_save, 8)
    # let_me_see.sub_cond_plot(PCA_data_frame, PCA_path, 4)
    #
    #
    # # Plot the results
    # print('Plotting and saving the results')
    # FA_path = save_path + r'\FA_model'
    # path_good = os.path.isdir(FA_path)
    # if not path_good:
    #     os.mkdir(FA_path)
    #
    # big_save = FA_path + r'\overview_plot'
    #
    # print('Scoring FA model')
    # #FA_scores = score_components(scaled_data, FA_path, 100, model_choice='FA')
    # # Need to add a section to let people determine the number of components for the FA model - DMJ
    # print('Running FA model')
    # chosen_num_of_dims = 24
    # FA_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points, chosen_num_of_dims, 'FA')
    # print('Reshaping data for seaborn')
    # FA_data_frame = prepare_data_for_seaborn(FA_data, bhv, trial_time[time_idx[0]:time_idx[1]], visual_trials, 'low_d')
    #
    # let_me_see.overview_plot(FA_data_frame, big_save, 8)
    # let_me_see.sub_cond_plot(FA_data_frame, FA_path, 4)
    #
    #
    # # Plot the results
    # print('Plotting and saving the results')
    # NMF_path = save_path + r'\NMF_model'
    # path_good = os.path.isdir(NMF_path)
    # if not path_good:
    #     os.mkdir(NMF_path)
    #
    # big_save = NMF_path + r'\overview_plot'
    #
    # #rint('Scoring NMF model')
    # #NMF_scores = score_components(scaled_data, NMF_path, 100, model_choice='NMF')
    #
    # print('Running NMF model')
    # chosen_num_of_dims = 20
    # NMF_data, model_name, dr_model = run_model(scaled_data, num_of_trials, num_of_time_points, chosen_num_of_dims, 'NMF')
    # print('Reshaping data for seaborn')
    # NMF_data_frame = prepare_data_for_seaborn(NMF_data, bhv, trial_time[time_idx[0]:time_idx[1]], visual_trials, 'low_d')
    #
    # let_me_see.overview_plot(NMF_data_frame, big_save, 8)
    # let_me_see.sub_cond_plot(NMF_data_frame, NMF_path, 4)

