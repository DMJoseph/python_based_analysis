import scipy.io as spio
import numpy as np
import sklearn.decomposition as skl
import matplotlib.pyplot as plt

trial_data = spio.loadmat(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_testData.mat', squeeze_me=True)

a = trial_data['VHS1_testData']
a = a[:, :, 26:]
num_of_trials = np.shape(a)[0]
num_of_neurons = np.shape(a)[1]
num_of_timepoints = np.shape(a)[2]
num_of_components = 20

print(f'Array is {num_of_trials} trials, {num_of_neurons} neurons, {num_of_timepoints} timepoints.')

reshapedData = np.empty((num_of_trials*num_of_timepoints, num_of_neurons))
for neuron in range(num_of_neurons):
    reshapedData[:, neuron] = np.ndarray.reshape(a[:, neuron, :], (num_of_timepoints * num_of_trials))

'''
sanityCheck = np.empty((num_of_trials*num_of_timepoints))
test_data = a[:, 0, :]
test_data = np.ndarray.reshape(test_data, (num_of_timepoints * num_of_trials))
for d in range(num_of_trials*num_of_timepoints):
    sanityCheck[d] = reshapedData[d, 0] in test_data
'''

print('Data reshaped, running PCA.')
pca_model = skl.PCA(n_components=num_of_components).fit(reshapedData)

print('Transforming data.')
transformed_data = pca_model.transform(reshapedData)
x_limits = None
y_limits = None

print('Plotting data')
if num_of_components > 1:
# Plot Data
    plt.scatter(transformed_data[:, 0], transformed_data[:, 1])
    ax = plt.gca()
    x_limits = ax.get_xlim()
    y_limits = ax.get_ylim()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(transformed_data[:, 1], transformed_data[:, 2], transformed_data[:, 3])

plt.show()