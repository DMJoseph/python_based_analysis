from data_preparation import load_data, get_vis_trials, reshape_data, normalise_data
from make_it_smaller import run_model
import numpy as np
import os
import tkinter as tk
from tkinter import filedialog
from data_loading.session_data import MatLabData, SimplifiedMatLabData

if __name__ == '__main__':

    root = tk.Tk()
    root.withdraw()

    # Ask for the folder to load the data from
    folder_path = filedialog.askdirectory()

    # Get subfolders and filenames
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)
        break

    # Check which level we're at:
    contents_test = []
    for ii in range(len(f)):
        contents_test.append('imgtrl_base_data' in f[ii])

    # If there are subfolders check them
    if dirnames:
        d = []
        dir1_contents_test = []
        for (subdirpath, subdirnames, subfilenames) in os.walk(os.path.join(dirpath, dirnames[0])):
            d.extend(subfilenames)
            break

        for ii in range(len(d)):
            dir1_contents_test.append('imgtrl_base_data' in d[ii])

    # Load in the data wherever we found it
    loaded_data = []
    if any(contents_test):
        loaded_data.append(MatLabData(folder_path))
    elif any(dir1_contents_test):
        for jj in range(len(dirnames)):
            # a = SimplifiedMatLabData(os.path.join(dirpath, dirnames[jj]), model_choice='NMF')
            loaded_data.append(MatLabData(os.path.join(dirpath, dirnames[jj])))

    basic_model = SimplifiedMatLabData(folder_path, model_choice='PCA', visual_filter='rewarded',
                                       attention_filter='all',
                                       laser_filter='all', neuron_group='sig_increasing')
    # basic_model.visualise_two_dimensions(dimensions_chosen=(1, 2))
    basic_model.visualise_three_dimensions(dimensions_chosen=(1, 2, 3))
