from sklearn import svm
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsClassifier
from data_preparation import load_data, get_vis_trials, reshape_data, normalise_data
from make_it_smaller import run_model
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
import numpy as np
from scipy import stats
import os
import tkinter as tk
from tkinter import filedialog
from data_loading.session_data import MatLabData, SimplifiedMatLabData
from scipy import stats
from statsmodels.stats.multitest import multipletests
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis



if __name__ == '__main__':

    root = tk.Tk()
    root.withdraw()

    folder_path = filedialog.askdirectory()
    f = []
    for (dirpath, dirnames, filenames) in os.walk(folder_path):
        f.extend(filenames)
        break

    # Check which level we're at:
    contents_test = []
    for ii in range(len(f)):
        contents_test.append('imgtrl_base_data' in f[ii])

    dir1_contents_test = []
    # d = []
    for dd in range(len(dirnames)):
        data_present = []
        for (subdirpath, subdirnames, subfilenames) in os.walk(os.path.join(dirpath, dirnames[dd])):
            # d.extend(subfilenames)
            if len(subfilenames) != 0:
                data_present.append('imgtrl_base_data' in subfilenames[0])
        dir1_contents_test.append(any(data_present))

    # dir1_contents_test = []
    # for ii in range(len(d)):
    #     dir1_contents_test.append('imgtrl_base_data' in d[ii])

    loaded_data = []
    if any(contents_test):
        loaded_data.append(MatLabData(folder_path))
    elif any(dir1_contents_test):
        for jj in range(len(dirnames)):
            # a = SimplifiedMatLabData(os.path.join(dirpath, dirnames[jj]), model_choice='NMF')
            if dir1_contents_test[jj]:
                origin_path = os.path.join(dirpath, dirnames[jj])
                loaded_data.append(MatLabData(origin_path))

    # Perform dimensionality reduction

    # Filter for the desired trial types
    # - Classify laser power in attend condition
    # - Test in alternative condition
    # - Try both ways


    for kk in range(len(loaded_data)):
        save_path = os.path.join(loaded_data[kk].matlab_data_path, 'LDA_test')
        if not os.path.isdir(save_path):
            os.makedirs(save_path)
        # NMF_path = save_path + r'\NMF_model.npy'

        # Filter it for only the visual trials
        filtered_data, visual_trials, time_idx = get_vis_trials(loaded_data[kk].trial_data, loaded_data[kk].bhv,
                                                                loaded_data[kk].trial_time)

        # Remove VIP cells
        non_vip_index = [x[0] != 'VIP' for x in loaded_data[kk].cell_label]
        filtered_data = filtered_data[:, non_vip_index, :]

        x_data_time = loaded_data[kk].trial_time[time_idx[0]:time_idx[1]]

        num_of_trials = np.shape(filtered_data)[0]
        num_of_neurons = np.shape(filtered_data)[1]
        num_of_time_points = np.shape(filtered_data)[2]

        # Baseline correct the data
        prestimData = np.nanmean(filtered_data[:, :, x_data_time <= 0], axis=2)
        prestimData = prestimData[:, :, np.newaxis]
        baseCorrectedData = filtered_data - prestimData

        # Get the trial average responses
        trial_means = np.nanmean(baseCorrectedData[:, :, x_data_time >= 0], axis=2)

        # Standard scale the data
        scaled_data = normalise_data(trial_means)

        sel = loaded_data[kk].bhv['sel']
        # Separate off the non-laser trials - split rel/irrel vis1/vis2 - can also try multi-output.
        stable_trials = loaded_data[kk].bhv['PERFstraight']['stableTrialIdx'][visual_trials, :]

        all_condition_labels = sel['condAll'][visual_trials]
        coarse_condition_labels = sel['condCoarse'][visual_trials]
        laser_powers = sel['laserPower'][visual_trials]
        # max_laser = laser_powers.max()
        unique_lasers = np.unique(laser_powers)
        max_laser = unique_lasers[-2]

        # Make trial labels
        trial_labels = np.zeros_like(coarse_condition_labels)

        # Attend no laser
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 1, laser_powers == 0), stable_trials[:, 1] == 1)] = 1
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 2, laser_powers == 0), stable_trials[:, 2] == 1)] = 1
        # Ignore no laser
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 3, laser_powers == 0), stable_trials[:, 3] == 1)] = 2
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 4, laser_powers == 0), stable_trials[:, 4] == 1)] = 2
        # Attend laser
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 1, laser_powers == max_laser), stable_trials[:, 1] == 1)] = 3
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 2, laser_powers == max_laser), stable_trials[:, 2] == 1)] = 3
        # Ignore laser
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 3, laser_powers == max_laser), stable_trials[:, 3] == 1)] = 4
        trial_labels[np.logical_and(np.logical_and(coarse_condition_labels == 4, laser_powers == max_laser), stable_trials[:, 4] == 1)] = 4

        bad_index = trial_labels == 0
        trial_labels = np.delete(trial_labels, bad_index)

        input_data = scaled_data[~bad_index, :]

        # no_laser_combi = np.logical_and(sel['combi2laser'][:, 1] == 0, np.logical_not(np.isnan(sel['combi2ori'][:, 1])))
        # no_laser_conds = sel['combi'][no_laser_combi, :]
        #
        # laser_combi = sel['combi2laser'][:, 1] == sel['combi2laser'][:, 1].max()
        # laser_conds = sel['combi'][laser_combi, :]



        clf = LinearDiscriminantAnalysis()
        ldaData = clf.fit_transform(input_data, trial_labels)
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        for tt in range(4):
            ax.scatter(ldaData[trial_labels == tt+1, 0], ldaData[trial_labels == tt+1, 1], ldaData[trial_labels == tt+1, 2], )
        plt.legend(['AN', 'IN', 'AL', 'IL'])
        plt.show()

        fig = plt.figure()
        for tt in range(4):
            plt.scatter(ldaData[trial_labels == tt + 1, 0], ldaData[trial_labels == tt + 1, 1])
        plt.legend(['AN', 'IN', 'AL', 'IL'])
        plt.title(loaded_data[kk].session_name)
        plt.show()

        fig = plt.figure()
        for tt in range(4):
            plt.scatter(ldaData[trial_labels == tt + 1, 1], ldaData[trial_labels == tt + 1, 2])
        plt.legend(['AN', 'IN', 'AL', 'IL'])
        plt.title(loaded_data[kk].session_name)
        plt.show()

        fig = plt.figure()
        for tt in range(4):
            plt.scatter(ldaData[trial_labels == tt + 1, 0], ldaData[trial_labels == tt + 1, 2])
        plt.legend(['AN', 'IN', 'AL', 'IL'])
        plt.title(loaded_data[kk].session_name)
        plt.show()


