import scipy.io as spio
import numpy as np
import sklearn.decomposition as skl
import matplotlib.pyplot as plt
from loading_mat_files import loadmat


def run_model(neural_data, score_components=True, max_components=50):
    
    #trial_data = loadmat(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_testData.mat')
    #behav_data = loadmat(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_bhv_data.mat')
    #bhv = behav_data['bhv_data']
    #np.unique(bhv['sel']['condCoarse'])

    #cond_coarse = cond_coarse['cond_coarse']
    #odour_trials = np.logical_or(cond_coarse == 5, cond_coarse == 6)
    #visual_trials = np.logical_not(odour_trials)
    #a = trial_data['VHS1_testData']
    #a = a[visual_trials, :, 26:]
    num_of_trials = np.shape(neural_data)[0]
    num_of_neurons = np.shape(neural_data)[1]
    num_of_timepoints = np.shape(neural_data)[2]
    
    print(f'Array is {num_of_trials} trials, {num_of_neurons} neurons, {num_of_timepoints} timepoints.')
    
    reshaped_data = np.empty((num_of_trials*num_of_timepoints, num_of_neurons))
    for neuron in range(num_of_neurons):
        reshaped_data[:, neuron] = np.ndarray.reshape(neural_data[:, neuron, :], (num_of_timepoints * num_of_trials))
    
    if score_components:
        fa_score = np.empty(max_components)
        print('Data reshaped, running FA.')
        for num_of_components in range(max_components):
            fa_model = skl.FactorAnalysis(n_components=num_of_components+1).fit(reshaped_data)
            fa_score[num_of_components] = fa_model.score(reshaped_data)
        
        fig = plt.figure()
        plt.plot(range(max_components), fa_score)
        plt.show()
    
    fa_model = skl.FactorAnalysis(n_components=12).fit(reshaped_data)
    
    print('Transforming data.')
    transformed_data = fa_model.transform(reshaped_data)
    
    reshaped_transform = np.empty((num_of_trials, 12, num_of_timepoints))
    for dim in range(12):
        reshaped_transform[:, dim, :] = np.ndarray.reshape(transformed_data[:, dim], (num_of_trials, num_of_timepoints))

    return reshaped_transform


