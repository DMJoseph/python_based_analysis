from import_preprocessed_data import ImportMatLabData
import os
import tkinter as tk
from tkinter import filedialog

'''
Short example function using ImportMatLabData to load a session exported from Spyglass into python. 
'''

root = tk.Tk()
root.withdraw()

folder_path = filedialog.askdirectory()
f = []
for (dirpath, dirnames, filenames) in os.walk(folder_path):
    f.extend(filenames)
    break

for ii in range(len(filenames)):
    session_path = os.path.join(dirpath, filenames[ii])

    if 'preprocessed_basic.mat' in session_path:
        mat_data = ImportMatLabData(session_path)

        print(mat_data.mismatch_trials['perfect_switch'])

