import numpy as np
import pandas as pd
from loading_mat_files import loadmat
from sklearn.preprocessing import StandardScaler

def load_data(data_path, behave_path, time_path):
    # Loads the trial by trial neural data, the behavioural data
    # and the time value of the trial time index of the neural data.

    trial_data = loadmat(data_path)
    trial_data = trial_data['trialData']
    behave_data = loadmat(behave_path)
    bhv = behave_data['bhv_data']
    trial_time = loadmat(time_path)
    trial_time = trial_time['trial_time']

    return trial_data, bhv, trial_time


def get_vis_trials(trial_data, bhv, trial_time, time_limits=None):

    if time_limits is None:
        time_limits = (-0.5, 1.5)

    cond_coarse = bhv['sel']['condCoarse']
    visual_trials = np.logical_and(cond_coarse != 5, cond_coarse != 6)
    time_idx = [np.nonzero(trial_time >= time_limits[0])[0][0], np.nonzero(trial_time >= time_limits[1])[0][0]]
    test_data = trial_data[visual_trials, :, time_idx[0]:time_idx[1]]

    return test_data, visual_trials, time_idx


def reshape_data(neural_data):
    num_of_trials = np.shape(neural_data)[0]
    num_of_neurons = np.shape(neural_data)[1]
    num_of_timepoints = np.shape(neural_data)[2]

    print(f'Array is {num_of_trials} trials, {num_of_neurons} neurons, {num_of_timepoints} time-points. Reshaping...')

    reshaped_data = np.empty((num_of_trials * num_of_timepoints, num_of_neurons))
    for neuron in range(num_of_neurons):
        reshaped_data[:, neuron] = np.ndarray.reshape(neural_data[:, neuron, :], (num_of_timepoints * num_of_trials))

    return reshaped_data


def prepare_data_for_seaborn(test_data, bhv, trial_time, trial_idx=None, data_type='Neural'):

    # An index of which trials should be included when making the data-frame.
    if trial_idx is None:
        trial_idx = np.tile(True, (np.shape(bhv['sel']['ori'])[0]))

    # Get the dimensions of the data
    num_of_trials = np.shape(test_data)[0]
    num_of_neurons = np.shape(test_data)[1]
    num_of_timepoints = np.shape(test_data)[2]

    # Turn each piece of trial information into a vector covering all the reshaped time-points.
    trial_nums = make_pandas_trial_data(np.nonzero(trial_idx)[0] + 1, num_of_trials, num_of_timepoints)
    orientation = make_pandas_trial_data(bhv['sel']['ori'][trial_idx], num_of_trials, num_of_timepoints)
    laser_power = make_pandas_trial_data(bhv['sel']['laserPower'][trial_idx], num_of_trials, num_of_timepoints)
    all_conditions = make_pandas_trial_data(bhv['sel']['condAll'][trial_idx], num_of_trials, num_of_timepoints)
    coarse_conditions = make_pandas_trial_data(bhv['sel']['condCoarse'][trial_idx], num_of_trials, num_of_timepoints)
    #outcome = make_pandas_trial_data(bhv['fsm']['outcome'][bhv['sel']['indexIntoFsm'][trial_idx] - 1], num_of_trials,
    #                                 num_of_timepoints)
    outcome = make_pandas_trial_data(bhv['PERFstraight']['correct'][trial_idx], num_of_trials, num_of_timepoints)

    trial_time = np.tile(trial_time, (num_of_trials, 1)).reshape((num_of_timepoints * num_of_trials))

    # Make an empty array to store the data in.
    reshaped_data = np.empty((num_of_trials * num_of_timepoints, num_of_neurons))

    if data_type == 'Neural':
        frames_to_join = [None] * num_of_neurons
    elif data_type == 'low_d':
        final_dataframe = pd.DataFrame({'trial': trial_nums,
                                       'ori': orientation,
                                       'laser': laser_power,
                                       'all_conds': all_conditions,
                                       'coarse_conds': coarse_conditions,
                                       'outcome': outcome,
                                       'time': trial_time,
                                       })

    for neuron in range(num_of_neurons):
        print(f'Adding neuron: {neuron + 1} of {num_of_neurons} to dataframe')

        reshaped_data[:, neuron] = np.ndarray.reshape(test_data[:, neuron, :], (num_of_timepoints * num_of_trials))

        if data_type == 'Neural':
            temp_df = pd.DataFrame({'trial': trial_nums,
                                    'ori': orientation,
                                    'laser': laser_power,
                                    'all_conds': all_conditions,
                                    'coarse_conds': coarse_conditions,
                                    'outcome': outcome,
                                    'time': trial_time,
                                    'signal': reshaped_data[:, neuron],
                                    'ROI': neuron,
                                    })

            frames_to_join[neuron] = temp_df
        elif data_type == 'low_d':
            final_dataframe[f'dim{neuron}'] = reshaped_data[:, neuron]

    if data_type == 'Neural':
        print('Concatenating all neurons')
        final_dataframe = pd.concat(frames_to_join)

    return final_dataframe


def make_pandas_trial_data(data, trial_num, timepoint_num):
    # Repeats the trial data so that there is one value for each time-point and then reshapes it into a vector.
    new_data = np.tile(data, (timepoint_num, 1)).transpose().reshape((timepoint_num * trial_num))

    return new_data


def normalise_data(data_to_norm):
    num_of_neurons = np.shape(data_to_norm)[1]

    scaled_data = np.empty(np.shape(data_to_norm))
    for neuron in range(num_of_neurons):
        scaler = StandardScaler()
        fit_neuron = scaler.fit_transform(data_to_norm[:, neuron].reshape(-1, 1))
        scaled_data[:, neuron] = fit_neuron.reshape(np.size(fit_neuron))

    return scaled_data
