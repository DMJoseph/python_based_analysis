import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn.decomposition as skl
import seaborn_test as st
from loading_mat_files import loadmat
from celluloid import Camera
from sklearn.manifold import TSNE





def run_fa(trial_data, bhv, trial_time, model_choice='FA'):

    # neural_df = st.prepare_data_for_seaborn(test_data, bhv, trial_time)
    if model_choice == 'TSNE':
        plot_tsne(test_data)
    else:
        reshaped_transform, model_choice = st.run_model(test_data, score_components=False, model_choice=model_choice)
        fa_df = st.prepare_data_for_seaborn(reshaped_transform, bhv, trial_time[time_idx[0]:time_idx[1]], visual_trials,
                                            data_type='low_d')

    return fa_df, model_choice


def visualise_low_d(fa_df, model_choice, plot_tag):
    sns.set(style="darkgrid")

    h = sns.pairplot(fa_df, vars=['dim0', 'dim1', 'dim2', 'dim3'], hue='coarse_conds', plot_kws=dict(s=20, alpha=0.5))
    h.savefig(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\all_coarse' + '_' + model_choice)

    for cond_idx in np.unique(fa_df.coarse_conds):
        h1 = sns.pairplot(fa_df.query(f'coarse_conds == {cond_idx}'), vars=['dim0', 'dim1', 'dim2', 'dim3'],
                          hue='all_conds', plot_kws=dict(s=20, alpha=0.5))

        # plt.title(f'Condition: {cond_idx}, hue: sub-condition')

        h2 = sns.pairplot(fa_df.query(f'coarse_conds == {cond_idx}'), vars=['dim0', 'dim1', 'dim2', 'dim3'],
                          hue='outcome', plot_kws=dict(s=20, alpha=0.5))

        # plt.title(f'Condition: {cond_idx}, hue: outcome')
        sub_cond_path = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\sig_plots_' + plot_tag
        path_good = os.path.isdir(sub_cond_path)
        if not path_good:
            os.mkdir(sub_cond_path)

        h1.savefig(sub_cond_path + r'\sub_cond' + str(cond_idx) + '_' + model_choice)
        h2.savefig(sub_cond_path + r'\outcome' + str(cond_idx) + '_' + model_choice)
    '''
    sns.relplot(x="dim0", y="dim1", sort=False, hue="outcome", estimator=None, units='trial', kind="line", data=fa_df.query("all_conds == 1"))
    sns.relplot(x="dim0", y="dim1", sort=False, hue="outcome", estimator=None, units='trial', kind="line", data=fa_df.query("all_conds == 1 or all_conds == 4 "))
    sns.relplot(x="dim0", y="dim1", z='dim2', sort=False, hue="outcome", estimator=None, units='trial', kind="line", data=fa_df.query("all_conds == 1 or all_conds == 4 "))

    sns.pairplot(fa_df.query('outcome == 1 or outcome == 2'), vars=['dim0', 'dim1', 'dim2', 'dim3'], hue='coarse_conds')

    sns.pairplot(fa_df.query('coarse_conds == 1 or coarse_conds == 2'), vars=['dim0', 'dim1', 'dim2', 'dim3'], hue='all_conds', plot_kws=dict(s=20, alpha=0.5))
    '''


def animate_trajectories(fa_df):
    fig = plt.figure()
    camera = Camera(fig)

    time_range = np.unique(fa_df['time'])
    for t in time_range:
        plot = sns.lineplot(x="dim0", y="dim1", sort=False, estimator=None, units='trial',
                            data=fa_df.query(f"all_conds == 1 and trial < 10 and time <= {t}"))

        camera.snap()

    anim = camera.animate(blit=False)
    anim.save(r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\animation.mp4 ')


def plot_tsne(trial_data, bhv, trial_time):

    num_of_trials = np.shape(test_data)[0]
    num_of_neurons = np.shape(test_data)[1]
    num_of_timepoints = np.shape(test_data)[2]

    condition_array = np.tile(cond_coarse[visual_trials], (num_of_timepoints, 1))
    labels = np.ndarray.reshape(condition_array, (num_of_timepoints * num_of_trials))
    reshaped_data = np.empty((num_of_trials * num_of_timepoints, num_of_neurons))

    for neuron in range(num_of_neurons):
        reshaped_data[:, neuron] = np.ndarray.reshape(test_data[:, neuron, :], (num_of_timepoints * num_of_trials))

    perp_values = [50, 5, 2]
    for perp in perp_values:
        # perform t-SNE
        tsne_model = TSNE(n_components=2, perplexity=perp, random_state=2020)

        embed = tsne_model.fit_transform(reshaped_data, labels)
        visualize_components(embed[:, 0], embed[:, 1], labels, show=False)
        plt.title(f"perplexity: {perp}")
        plt.show()


def visualize_components(component1, component2, labels, show=True):
    """
        Plots a 2D representation of the data for visualization with categories
        labelled as different colors.

        Args:
          component1 (numpy array of floats) : Vector of component 1 scores
          component2 (numpy array of floats) : Vector of component 2 scores
          labels (numpy array of floats)     : Vector corresponding to categories of
                                               samples

        Returns:
          Nothing.

        """

    plt.figure()
    cmap = plt.cm.get_cmap('tab10')
    plt.scatter(x=component1, y=component2, c=labels, cmap=cmap)
    plt.xlabel('Component 1')
    plt.ylabel('Component 2')
    plt.colorbar(ticks=range(10))
    plt.clim(-0.5, 9.5)
    if show:
        plt.show()


plot_tag = '0001'
data_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_spiking_0001.mat'
bhv_data_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\VHS1_bhv_data.mat'
time_p = r'C:\Users\Dylan Myers-Joseph\Documents\PhD\Test_data\VHS001_20200129\trial_time.mat'

# Commented out temporarily for the TSNE
#df_fa, model_choice = run_fa(data_p, bhv_data_p, time_p, model_choice='PCA')
#visualise_low_d(df_fa, model_choice, plot_tag)

plot_tsne(trial_data, bhv, trial_time)


# anim = animate_trajectories(df_fa)
