



def get_full_pca(data, number_of_components, display_pcs=False):
    # Get Data Structure
    number_of_timepoints = np.shape(data)[0]
    number_of_trials = np.shape(data)[1]
    number_of_pixels = np.shape(data)[2]

    # Reshape Data
    data = np.ndarray.reshape(data, (number_of_timepoints * number_of_trials, number_of_pixels))

    # Perform PCA
    pca_model = PCA(n_components=number_of_components).fit(data)

    transformed_data = pca_model.transform(data)
    x_limits = None
    y_limits = None

    if display_pcs:
        if number_of_components > 1:
            # Plot Data
            plt.scatter(transformed_data[:, 1], transformed_data[:, 2])
            ax = plt.gca()
            x_limits = ax.get_xlim()
            y_limits = ax.get_ylim()


